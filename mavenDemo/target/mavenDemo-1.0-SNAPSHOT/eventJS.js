/*先获取输入框的对象*/
var usernameInput = document.getElementById("username");
/*绑定事件*/
usernameInput.onblur = checkUsername;
    function checkUsername() {
    /*获取输入的内容，并去除空格*/
    var username = usernameInput.value.trim();
    /*正则判断*/
        var reg = /^\w{3,8}$/;
    /*判断用户的长度*/
    if(reg.test(username)){
        document.getElementById("user_err").style.display = 'none';
        return true;
    }else {
        document.getElementById("user_err").style.display = 'inline';
        return false;
    }
}
var passwordInput = document.getElementById("password");
/*绑定事件*/
passwordInput.onblur = checkPassword;
    function checkPassword() {
    /*获取输入的内容，并去除空格*/
    var password = passwordInput.value.trim();
    var reg = /^\w{3,8}$/;
    /*判断用户的长度*/
    if(reg.test(password)){
        document.getElementById("pass_err").style.display = 'none';
        return true;
    }else {
        document.getElementById("pass_err").style.display = 'inline';
        return false;
    }
}

var regForm = document.getElementById("submit");
regForm.onsubmit = function (){
    return checkUsername() && checkPassword();
}