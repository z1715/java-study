create database mybatis;
use mybatis;

drop table if exists tb_user;

create table tb_user(
    id int primary key auto_increment,
    username varchar(20),
    password varchar(20),
    gender char(1),
    addr varchar(30)
);

insert into tb_user values ('jack','234324','男','ksdjlfksjlfj');
insert into tb_user values ('rose','234324','女','ksdjlfksjlfdsf');
insert into tb_user values ('kenny','234324','男','ksdjlfksjlfjdsf');
