import mapper.BrandMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import pojo.Brand;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestMybatis {
    @Test
    public void testSelectAll() throws Exception {
        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);
        List<Brand> brands = brandMapper.selectAll();
        System.out.println(brands);

        //4,关闭资源
        sqlSession.close();

    }

    @Test
    public void testSelectById() throws Exception {
        //接收参数
        int id = 2;

        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        Brand brand = brandMapper.selectById(id);
        System.out.println(brand);

        //4,关闭资源
        sqlSession.close();

    }

    @Test
    public void testSelectByCondition() throws Exception {
        //接收参数
        int status = 0;
        String companyName = "东云";
        String brandName = "";

        companyName = "%" + companyName + "%";
        brandName = "%" + brandName + "%";
        /*
        封装成对象,传给方法
        Brand brand = new Brand();
        brand.setStatus(status);
        brand.setCompanyName(companyName);
        brand.setBrandName(brandName);
*/
        //用map集合
        Map map = new HashMap();
        map.put("status", status);
        map.put("companyName", companyName);
        map.put("brandName", brandName);


        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);


        List<Brand> brand = brandMapper.selectByCondition(status, brandName, companyName);
        List<Brand> brands = brandMapper.selectByChooseCondition(map);
        System.out.println(brand);
        System.out.println(brands);

        //4,关闭资源
        sqlSession.close();

    }

    //动态选择数据
    @Test
    public void testSelectByChooseCondition() throws Exception {
        //接收参数
        int status = 0;
        String companyName = "东云";
        String brandName = "";

        companyName = "%" + companyName + "%";
        brandName = "%" + brandName + "%";
        /*
        封装成对象,传给方法
        Brand brand = new Brand();
        brand.setStatus(status);
        brand.setCompanyName(companyName);
        brand.setBrandName(brandName);
*/
        //用map集合
        Map map = new HashMap();
        map.put("status", status);
        map.put("companyName", companyName);
        map.put("brandName", brandName);


        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);


        List<Brand> brands = brandMapper.selectByChooseCondition(map);
        System.out.println(brands);

        //4,关闭资源
        sqlSession.close();

    }

    //添加数据
    @Test
    public void testAdd() throws Exception {
        //接收参数
        int status = 0;
        String companyName = "超人搬家";
        String brandName = "超人";
        int orderBy = 110;
        String description = "快的超乎你想象";


        //封装成对象,传给方法
        Brand brand = new Brand();
        brand.setStatus(status);
        brand.setCompanyName(companyName);
        brand.setBrandName(brandName);
        brand.setDescription(description);
        brand.setOrderBy(orderBy);


        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象（）括号里传入true可以自动提交事务
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);


        brandMapper.add(brand);
        //需要提交事务才能把数据添加到数据库中
        sqlSession.commit();
        //4,关闭资源
        sqlSession.close();

    }

    //获取添加的数据的主键返回值
    @Test
    public void testAddItem() throws Exception {
        //接收参数
        int status = 0;
        String companyName = "海王物流";
        String brandName = "海王";
        int orderBy = 110;
        String description = "绝对安全";


        //封装成对象,传给方法
        Brand brand = new Brand();
        brand.setStatus(status);
        brand.setCompanyName(companyName);
        brand.setBrandName(brandName);
        brand.setDescription(description);
        brand.setOrderBy(orderBy);


        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象（）括号里传入true可以自动提交事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);


        brandMapper.addItem(brand);
        System.out.println(brand.getId());
        //需要提交事务才能把数据添加到数据库中
        //sqlSession.commit();
        //4,关闭资源
        sqlSession.close();

    }

    //修改数据
    @Test
    public void testUpdate() throws Exception {
        //接收参数
        int status = 0;
        String companyName = "海王物流";
        String brandName = "海王";
        int orderBy = 110;
        String description = "可能有点湿";
        int id = 7;


        //封装成对象,传给方法
        Brand brand = new Brand();
        brand.setStatus(status);
        brand.setCompanyName(companyName);
        brand.setBrandName(brandName);
        brand.setDescription(description);
        brand.setOrderBy(orderBy);
        brand.setId(id);


        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象（）括号里传入true可以自动提交事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);


        int update = brandMapper.update(brand);
        System.out.println(update);
        //需要提交事务才能把数据添加到数据库中
        //sqlSession.commit();
        //4,关闭资源
        sqlSession.close();

    }

    //修改动态数据
    @Test
    public void testUpdateDynamic() throws Exception {
        //接收参数

        String companyName = "海王叉子物流";

        int id = 7;


        //封装成对象,传给方法
        Brand brand = new Brand();
        brand.setCompanyName(companyName);

        brand.setId(id);


        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象（）括号里传入true可以自动提交事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);


        int update = brandMapper.updateDynamic(brand);
        System.out.println(update);
        //需要提交事务才能把数据添加到数据库中
        //sqlSession.commit();
        //4,关闭资源
        sqlSession.close();

    }

    //删除数据
    @Test
    public void testDelete() throws Exception {
        //接收参数

        int id = 7;


        //封装成对象,传给方法
        Brand brand = new Brand();

        brand.setId(id);


        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象（）括号里传入true可以自动提交事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);


         brandMapper.deleteById(id);

        //需要提交事务才能把数据添加到数据库中
        //sqlSession.commit();
        //4,关闭资源
        sqlSession.close();

    }

    //批量删除数据
    @Test
    public void testDeleteByIds() throws Exception {
        //接收参数

        int[] id = {8,9};

        //1,获取SqlSessionFactory对象
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //2，SqlSession对象（）括号里传入true可以自动提交事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);

        //3，获取Mapper的接口代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);
        //ParamNameResolver

        brandMapper.deleteByIds(id);

        //需要提交事务才能把数据添加到数据库中
        //sqlSession.commit();
        //4,关闭资源
        sqlSession.close();

    }
}
