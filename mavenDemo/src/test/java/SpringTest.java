import SpringStudyDemo.SpringConfiguration;
import SpringStudyDemo.UserDao;
import SpringStudyDemo.UserService;
import com.alibaba.druid.pool.DruidDataSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * @author
 * @version 1.0.0
 * @ClassName SpringTest
 * @Description  测试Spring集成Junit
 * @createTime 2022/1/4  09:56:49
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfiguration.class})
public class SpringTest {
    @Resource
    private UserDao userDao;
    @Resource
    private UserService userService;
    @Resource
    private DruidDataSource dataSource;

    @Test
    public void test6() throws SQLException {
        System.out.println(dataSource.getConnection());
    }
    @Test
    public void test5(){
        userService.save();
    }
    @Test
    public void test4(){
        userDao.save();
    }
    @Test
    public void test1() {
        ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao user = (UserDao) app.getBean("userDao");
        user.save();
        ((ClassPathXmlApplicationContext)app).close();
        System.out.println(user);
    }

    //读取配置文件
    @Test
    public void test2() {
        ResourceBundle jdbc = ResourceBundle.getBundle("jdbc");
        String username = jdbc.getString("jdbc.username");
        String age = jdbc.getString("jdbc.age");
        String addr = jdbc.getString("jdbc.addr");
        System.out.println(username);
        System.out.println(age);
        System.out.println(addr);
    }

    //
    @Test
    public void test3() {
        ResourceBundle jdbc = ResourceBundle.getBundle("jdbc");
        String username = jdbc.getString("jdbc.username");
        String password = jdbc.getString("jdbc.password");
        String driver = jdbc.getString("jdbc.driver");
        String age = jdbc.getString("jdbc.age");
        String url = jdbc.getString("jdbc.url");
        DruidDataSource source = new DruidDataSource();
        source.setDriverClassName(driver);
        source.setPassword(password);
        source.setUsername(username);
        source.setUrl(url);
    }


}
