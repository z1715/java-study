<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/12/25
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录</title>
</head>
<body>

<div id="loginDiv">
    <form action="/mavenDemo_war_exploded/loginServlet" method="post" id="form">
        <h1 id="loginMsg">登录</h1>
        <h6>${login_err}</h6>
        <p>用户名：<input id="username" name="username" type="text" value="${cookie.username.value}"></p>
        <p>密码：<input id="password" name="password" type="password" value="${cookie.password.value}"></p>
        <p>记住我：<input id="remember" name="remember" value="1" type="checkbox"></p>
        <div id="subDiv">
            <input type="submit" class="button" value="登录">
            <input type="reset" class="button" value="我要重新输入">
            <a href="register.jsp">注册账号</a>
        </div>
    </form>
</div>

</body>
</html>
