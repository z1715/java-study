<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/12/25
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>注册</title>
</head>
<body>

<div id="registerDiv">
    <form action="/mavenDemo_war_exploded/registerServlet" method="post" id="form">
        <h1 id="loginMsg">注册</h1>
        <h6>${register_err}</h6>
        <p>用户名：<input id="username" name="username" type="text" ></p>
        <p>密码：<input id="password" name="password" type="password" ></p>
        <div id="subDiv">
            <input type="submit" class="button" value="注册">
            <input type="reset" class="button" value="我要重新输入">
            <a href="login.jsp">我要登录</a>
        </div>
    </form>
</div>

</body>
</html>
