<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/12/23
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html >
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>jstlIfAndForeach</title>
</head>
<body>
<table border="1" cellspacing="0" width="800">
    <tr>
        <th>序号</th>
        <th>品牌名称</th>
        <th>企业名称</th>
        <th>排序</th>
        <th>品牌介绍</th>
        <th>状态</th>
        <th>操作</th>
    </tr>

    <c:forEach items="${brands}" var="brand" varStatus="status">
        <tr>
                <%--<td>${brand.id}</td>用下面的方法便于列开头的序号--%>
            <td>${status.count}</td>
            <td>${brand.brandName}</td>
            <td>${brand.companyName}</td>
            <td>${brand.orderBy}</td>
            <td>${brand.description}</td>
            <c:if test="${brand.status==1}">
                <td>启用</td>
            </c:if>
            <c:if test="${brand.status==0}">
                <td>禁用</td>
            </c:if>
            <td><a href="/mavenDemo_war_exploded/updateServlet?id=#{brand.id}">修改</a>
                <a href="/mavenDemo_war_exploded/deleteServlet?id=#{brand.id}">删除</a> </td>
        </tr>
    </c:forEach>

</table>
</body>
</html>
