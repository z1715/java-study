import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author
 * @version 1.0.0
 * @ClassName ServletDemo1
 * @Description
 * @createTime 2021/12/17  19:05:58
 */
@WebServlet("/demo2")
public class ServletDemo1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //String getMethod():获取请求方式（如get、post）
        String method = req.getMethod();
        System.out.println("这是请求方式：" + method);
        //String getContextPath():动态获取虚拟目录（项目访问路径）
        String contextPath = req.getContextPath();
        System.out.println("这是项目访问路径：" + contextPath);
        //StringBuffer getRequestURL():获取URL（统一资源定位符）
        StringBuffer requestURL = req.getRequestURL();
        System.out.println("这是url：" + requestURL);
        //String getRequestURI():获取URI（统一资源标识符）
        String requestURI = req.getRequestURI();
        System.out.println("这是uri：" + requestURI);
        //String getQueryString():获取请求参数（GET方式）
        String queryString = req.getQueryString();
        System.out.println("这是请求参数：" + queryString);

        //获取请求头:user-agent(浏览器的版本信息)
        String header = req.getHeader("user-agent");
        System.out.println(header);
        System.out.println("doGet`````````");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost```````");
    }
}
