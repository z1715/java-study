package mapper;

import org.apache.ibatis.annotations.Param;
import pojo.User;

import java.util.List;

public interface UserMaer {
    List<User> selectAll();
    List<User> selectOne();
    User query(@Param("username") String username, @Param("password") String password);

    User selectByUsername(@Param("username") String username);

    void add(User user);
}
