package mapper;

import org.apache.ibatis.annotations.Param;
import pojo.Brand;

import java.util.List;
import java.util.Map;

public interface BrandMapper {

    List<Brand> selectAll();

    //根据id查看某个行的信息
    Brand selectById(int id);

    //条件查询
    /**
     * 1，如果方法中需要传入多个参数，需要使用@Param("SQL参数占位符名称")
     * 2，对象参数：对象的属性名称要和SQL参数占位符名称一致
     */
    List<Brand> selectByCondition(@Param("status") int status, @Param("companyName") String companyName, @Param("brandName") String brandName);

    List<Brand> selectByCondition(Map map);

    List<Brand> selectByCondition(Brand brand);

    //单条件动态查询
    List<Brand> selectByChooseCondition(Map map);

    //添加
    void add(Brand brand);

    void addItem(Brand brand);

    //修改
    int update(Brand brand);

    //动态修改
    int updateDynamic(Brand brand);

    //删除
    void deleteById(int id);

    //批量删除
    void deleteByIds(int[] ids);

}
