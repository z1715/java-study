import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @author
 * @version 1.0.0
 * @ClassName ServerDemo
 * @Description 自定义服务器
 * @createTime 2021/12/16  12:48:02
 */
public class ServerDemo {
    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(9099);
        System.out.println("服务器开始运行······");
        while (true){
            Socket socket = serverSocket.accept();
            System.out.println("来自：[" + socket.getRemoteSocketAddress() +"]的连接");
            Thread t = new Handlers(socket);
            t.start();
        }
    }
}
class Handlers extends Thread{
    Socket socket;
    public Handlers(Socket socket){
        this.socket=socket;
    }
    public void run(){
        try(InputStream inputStream = this.socket.getInputStream()){
            try (OutputStream outputStream = this.socket.getOutputStream()){
                handle(inputStream, outputStream);

            }
        }catch (Exception e ){
            try {
                this.socket.close();
            }catch (IOException ioException){
            }
            System.out.println("客户端失去连接········");
        }
    }
    private void handle(InputStream input,OutputStream output) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8));
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output, StandardCharsets.UTF_8));
        //读取http请求
        boolean requestOk = false;
        String first = reader.readLine();
        if (first.startsWith("GET / HTTP/1.")){
            requestOk = true;
        }
        for (;;){
            String header = reader.readLine();
            if (header.isEmpty()){
                break;
            }
            System.out.println(header);
        }
        if (!requestOk){
            //发送错误响应
            writer.write("HTTP/1.0 404 Not Found\r\n");
            writer.write("Content-Length:0\r\n");
            writer.write("\r\n");
            writer.flush();
        }else {
            //发送成功响应
            //读取html文件，转换为字符串
            BufferedReader br = new BufferedReader(new FileReader("E:\\JS\\js\\mavenDemo\\src\\main\\Web\\1-login.html"));
            StringBuilder data = new StringBuilder();
            String line = null;
            while ((line = br.readLine()) != null){
                data.append(line);

            }
            br.close();
            int length = data.toString().getBytes(StandardCharsets.UTF_8).length;

            writer.write("HTTP/1.1 200 OK\r\n");
            writer.write("Connection:keep-alive\r\n");
            writer.write("Content-Type:text/html\r\n");
            writer.write("Content-Length:" + length + "\r\n");
            writer.write("\r\n");
            writer.write(data.toString());
            writer.flush();
        }
    }

}
