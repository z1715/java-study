package factory;

import SpringStudyDemo.UserDao;
import SpringStudyDemo.UserDaoImpl;
import pojo.User;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserDaoFactory
 * @Description  一个静态工厂类
 * @createTime 2022/1/4  09:42:43
 */
public class UserDaoFactory {
    public static UserDao getUser(){
        return new UserDaoImpl();
    }

    public UserDao getUserDynamic(){
        return new UserDaoImpl();
    }
}
