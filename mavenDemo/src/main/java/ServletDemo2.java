import pojo.Brand;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @version 1.0.0
 * @ClassName ServletDemo2
 * @Description 练习jstl
 * @createTime 2021/12/23  15:13:36
 */
@WebServlet("/jstl1")
public class ServletDemo2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1,准备数据
        List<Brand> brands = new ArrayList<>();
        brands.add(new Brand(1,"朋在","有朋自在",100,"谢谢",1));
        brands.add(new Brand(2,"中朋在","中有朋自在",100,"谢谢",0));
        brands.add(new Brand(3,"朋是在","有朋是自在",100,"谢谢",1));
        //2,把数据存储到域中
        req.setAttribute("brands",brands);
        req.setAttribute("status",1);

        System.out.println("数据存好了");
        //3，转发到jstl-if.jsp
        req.getRequestDispatcher("/jstlIf.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
