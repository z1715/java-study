package web.filter;
/**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description  登录验证的过滤器
 * @createTime 2021/12/26  16:47:44
 */
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "LoginFilter",urlPatterns = "")
public class LoginFilter implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        //如果不过滤，会无法加载css、js等资源，所以要给一些必须不能被过滤的路径放行，文件夹用"/xxxx/"
        String[] urls = {"/selectAllByJsonServlet","/brand.html","/login.jsp", "/loginServlet", "/register.jsp", "/registerServlet","/index.html"};
        HttpServletRequest req = (HttpServletRequest) request;
        String requestURL = req.getRequestURL().toString();
        for (String url : urls) {
            if (requestURL.contains(url)){
                chain.doFilter(request, response);
                return;
            }

        }

        HttpSession session = req.getSession();
        Object user = session.getAttribute("user");
        if (user!=null){
            chain.doFilter(request, response);
        }else {
            req.setAttribute("login_err","要先登录啊");
            req.getRequestDispatcher("/login.jsp").forward(req,response);
        }
    }
}
