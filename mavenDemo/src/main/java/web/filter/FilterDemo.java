package web.filter; /**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description
 * @createTime 2021/12/26  14:16:58
 */

import javax.servlet.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebFilter("")
public class FilterDemo implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        System.out.println("过滤器启动······");
        //放行前，对request进行处理
        chain.doFilter(request, response);
        //放行后，对response进行处理

    }
}
