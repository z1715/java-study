package web; /**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description
 * @createTime 2021/12/24  21:26:53
 */

import service.BrandService;
import service.Impl.BrandServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "deleteServlet", value = "/deleteServlet")
public class DeleteServlet extends HttpServlet {
    BrandService service = new BrandServiceImpl();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        service.delete(Integer.parseInt(id));

        request.getRequestDispatcher("selectAllServlet").forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
