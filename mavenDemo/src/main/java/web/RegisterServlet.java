package web;

import mapper.UserMaer;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import pojo.User;
import service.UserService;
import utilsDemo.SqlSessionFactoryUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author
 * @version 1.0.0
 * @ClassName web.RegisterServlet
 * @Description
 * @createTime 2021/12/22  09:36:44
 */
@WebServlet("/registerServlet")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1,接收数据，封装对象
        req.setCharacterEncoding("UTF-8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);

        UserService userService = new UserService();
        boolean register = userService.register(user);
        if (register) {
            //提交事务
            String contextPath = req.getContextPath();
            //当前接收的请求与重定向后的请求是两个请求，又为了安全性，所以要用session存登录成功后的对象
            HttpSession session = req.getSession();
            session.setAttribute("user",user);
            resp.sendRedirect(contextPath + "/selectAllServlet");
        } else {
            req.setAttribute("register_err","用户名已经被注册了");
            req.getRequestDispatcher("/register.jsp").forward(req,resp);
           /* //设置中文正常显示
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().write("用户名已存在");*/
        }
        //释放资源


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
