package web; /**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description 根据id查找对应数据
 * @createTime 2021/12/25  07:46:28
 */

import pojo.Brand;
import service.BrandService;
import service.Impl.BrandServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "selectByIdServlet", value = "/selectByIdServlet")
public class SelectByIdServlet extends HttpServlet {
    private final BrandService service = new BrandServiceImpl();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        Brand brand = service.selectById(Integer.parseInt(id));
        
        request.setAttribute("brand",brand);

        request.getRequestDispatcher("/update.jsp").forward(request,response);
        
        
        
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
