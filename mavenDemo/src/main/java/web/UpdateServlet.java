package web;
/**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description 更新数据
 * @createTime 2021/12/24  20:52:33
 */

import pojo.Brand;
import service.BrandService;
import service.Impl.BrandServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "updateServlet", value = "/updateServlet")
public class UpdateServlet extends HttpServlet {
    private final BrandService service = new BrandServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //处理post请求的中文乱码问题
        request.setCharacterEncoding("utf-8");

        String id = request.getParameter("id");
        String brandName = request.getParameter("brandName");
        String companyName = request.getParameter("companyName");
        String orderBy = request.getParameter("orderBy");
        String description = request.getParameter("description");
        String status = request.getParameter("status");
        Brand brand = new Brand();
        brand.setId(Integer.parseInt(id));
        brand.setBrandName(brandName);
        brand.setCompanyName(companyName);
        brand.setOrderBy(Integer.parseInt(orderBy));
        brand.setDescription(description);
        brand.setStatus(Integer.parseInt(status));

        service.update(brand);
        request.getRequestDispatcher("/selectAllServlet").forward(request, response);
        System.out.println("1234");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
