package web;

import mapper.UserMaer;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import pojo.User;
import service.UserService;
import utilsDemo.SqlSessionFactoryUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author
 * @version 1.0.0
 * @ClassName web.LoginServlet
 * @Description
 * @createTime 2021/12/21  11:01:34
 */
@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1,接收用户名和密码
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        //记录用户的”记住我“的状态
        String remember = req.getParameter("remember");

        UserService userService = new UserService();
        User user = userService.login(username, password);

        resp.setContentType("text/html;charset=utf-8");
        //3,如果rs是null说明不存在该对象，登录失败
        if (user!=null){
            //登录成功，用的是重定向的方式
            if ("1".equals(remember)){
                //创建Cookie对象，
                Cookie username1 = new Cookie("username", username);
                Cookie password1 = new Cookie("password", password);
                //设置Cookie存活时间
                username1.setMaxAge(60*60*24*7);
                password1.setMaxAge(60*60*24*7);
                //发送Cookie
                resp.addCookie(username1);
                resp.addCookie(password1);
            }
            String contextPath = req.getContextPath();
            //当前接收的请求与重定向后的请求是两个请求，又为了安全性，所以要用session存登录成功后的对象
            HttpSession session = req.getSession();
            session.setAttribute("user",user);
            resp.sendRedirect(contextPath + "/selectAllServlet");
        }else {
            //登录失败，用的是转发的方式
            req.setAttribute("login_err","用户名或密码错误！");
            req.getRequestDispatcher("/login.jsp").forward(req,resp);
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
