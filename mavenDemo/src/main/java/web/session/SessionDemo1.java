package web.session; /**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description
 * @createTime 2021/12/25  15:54:18
 */

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "sessionDemo1", value = "/sessionDemo1")
public class SessionDemo1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取Session对象
        HttpSession session = request.getSession();
        session.setAttribute("username","红蒂");
        session.setAttribute("password",146);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
