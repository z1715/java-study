package web; /**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description  查看所有数据
 * @createTime 2021/12/24  20:08:37
 */

import com.alibaba.fastjson.JSON;
import pojo.Brand;
import service.BrandService;
import service.Impl.BrandServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "selectAllByJsonServlet", value = "/selectAllByJsonServlet")
public class SelectAllByJsonServlet extends HttpServlet {
    private final BrandService brandService = new BrandServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Brand> brands = brandService.selectAll();

        //将集合转换为json数据（序列化）
        String s = JSON.toJSONString(brands);

        //响应数据
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().write(s);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request,response);
    }
}
