package web; /**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description 替换HttpServlet，根据请求的最后一段路径来进行方法的分发
 * @createTime 2021/12/30  16:27:29
 */

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.lang.reflect.Method;

public class BaseServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1，获取请求路径
        String requestURI = req.getRequestURI();
        //2，获取最后一段路径，也就是方法名
        int index = requestURI.lastIndexOf("/");
        String substring = requestURI.substring(index + 1);
        //3，获取BaseServlet的子类的字节码对象Class（哪个子类调用this，this就代表哪个子类）
        Class<? extends BaseServlet> aClass = this.getClass();
        try {
            Method method = aClass.getMethod(substring, HttpServletRequest.class, HttpServletResponse.class);
           method.invoke(this, req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
