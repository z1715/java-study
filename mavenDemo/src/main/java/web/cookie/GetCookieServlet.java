package web.cookie; /**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description 测试Cookie
 * @createTime 2021/12/25  12:02:54
 */

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.net.URLDecoder;

@WebServlet(name = "getCookieServlet", value = "/getCookieServlet")
public class GetCookieServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("username")){
                //URL解码
                String decode = URLDecoder.decode(cookie.getValue(), "UTF-8");
                System.out.println(cookie.getName() + ":" +decode);
            }
            
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
