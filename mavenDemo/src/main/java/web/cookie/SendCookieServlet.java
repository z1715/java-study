package web.cookie; /**
 * @ClassName ${NAME}
 * @author
 * @version 1.0.0
 * @Description  创建Cookie
 * @createTime 2021/12/25  12:00:54
 */

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet(name = "sendCookieServlet", value = "/sendCookieServlet")
public class SendCookieServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Cookie存储中文，先编码
        String value = "杰哥";
        String encode = URLEncoder.encode(value, "UTF-8");

        Cookie cookie = new Cookie("username", encode);
        //设置Cookie存活时间，以秒为单位；设置正数是cookie在硬盘里存活指定时间，0是删除Cookie，负数是在浏览器关闭前
        cookie.setMaxAge(60);
        response.addCookie(cookie);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
