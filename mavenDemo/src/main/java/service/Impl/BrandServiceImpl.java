package service.Impl;

import mapper.BrandMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import pojo.Brand;
import service.BrandService;
import utilsDemo.SqlSessionFactoryUtil;

import java.util.List;

/**
 * @author
 * @version 1.0.0
 * @ClassName BrandServiceImpl
 * @Description
 * @createTime 2021/12/30  10:47:29
 */
public class BrandServiceImpl implements BrandService {
    private final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryUtil.getSqlSessionFactory();

    //查看所有数据
    @Override
    public List<Brand> selectAll() {
        SqlSession session = sqlSessionFactory.openSession();
        BrandMapper mapper = session.getMapper(BrandMapper.class);
        List<Brand> brands = mapper.selectAll();
        session.close();
        return brands;
    }

    //根据id查看数据，可以做数据回显
    @Override
    public Brand selectById(int id) {
        SqlSession session = sqlSessionFactory.openSession();
        BrandMapper mapper = session.getMapper(BrandMapper.class);
        Brand brand = mapper.selectById(id);
        session.close();

        return brand;
    }

    //添加数据
    @Override
    public void add(Brand brand) {
        SqlSession session = sqlSessionFactory.openSession();
        BrandMapper mapper = session.getMapper(BrandMapper.class);
        mapper.add(brand);
        session.commit();
        session.close();
    }

    //修改数据
    @Override
    public void update(Brand brand) {
        SqlSession session = sqlSessionFactory.openSession();
        BrandMapper mapper = session.getMapper(BrandMapper.class);
        int update = mapper.update(brand);

        session.commit();
        session.close();
    }

    //删除数据
    @Override
    public void delete(int id) {
        SqlSession session = sqlSessionFactory.openSession();
        BrandMapper mapper = session.getMapper(BrandMapper.class);
        mapper.deleteById(id);
        session.commit();
        session.close();
    }

    @Override
    public void deleteByIds(int[] ids) {
        SqlSession session = sqlSessionFactory.openSession();
        BrandMapper mapper = session.getMapper(BrandMapper.class);
        mapper.deleteByIds(ids);
        session.commit();
        session.close();
    }

}
