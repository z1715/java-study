package service;

import pojo.Brand;

import java.util.List;

/**
 * @author
 * @version 1.0.0
 * @ClassName BrandService
 * @Description  Brand的service层
 * @createTime 2021/12/24  19:11:12
 */
public interface BrandService {

    //查看所有数据
     List<Brand> selectAll();
    //根据id查看数据，可以做数据回显
     Brand selectById(int id);

     //添加数据
    void add(Brand brand);

    //修改数据
     void update(Brand brand);
    //删除数据
     void delete(int id);
     void deleteByIds(int[] ids);
}
