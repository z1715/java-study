package service;

import mapper.UserMaer;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import pojo.User;
import utilsDemo.SqlSessionFactoryUtil;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserService
 * @Description
 * @createTime 2021/12/25  17:43:43
 */
public class UserService {
    private final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryUtil.getSqlSessionFactory();

    //查询用户是否存在，如果返回null，说明用户不存在，也就是用户名或密码错了
    public User login(String username,String password){
        SqlSession session = sqlSessionFactory.openSession();
        UserMaer mapper = session.getMapper(UserMaer.class);

        User user = mapper.query(username, password);
        session.close();

        return user;
    }
    //注册方法
    public boolean register(User user){
        SqlSession session = sqlSessionFactory.openSession();
        UserMaer mapper = session.getMapper(UserMaer.class);

        User user1 = mapper.selectByUsername(user.getUsername());
        /*if (user==null){
            User user1 = new User();
            user1.setUsername(username);
            user1.setPassword(password);
            mapper.add(user1);
            session.commit();
            session.close();

            return true;
        }else {
            session.close();
            return false;
        }*/
        //比注释的代码更好的写法
        if (user1==null){

            mapper.add(user);
            session.commit();
        }
        session.close();
        return user1==null;


    }

}
