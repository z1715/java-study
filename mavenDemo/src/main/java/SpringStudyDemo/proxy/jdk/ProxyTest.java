package SpringStudyDemo.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author
 * @version 1.0.0
 * @ClassName ProxyTest
 * @Description
 * @createTime 2022/1/26  14:20:53
 */
public class ProxyTest {
    public static void main(String[] args) {

        //增强对象
        Enhance enhance = new Enhance();
        //目标对象
        Target target = new Target();


        TargetInterface o = (TargetInterface) Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                (proxy, method, args1) -> {
                    enhance.before();
                    Object invoke = method.invoke(target, args1);
                    enhance.after();
                    return invoke;
                }
        );

        o.save();

    }
}
