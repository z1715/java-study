package SpringStudyDemo.proxy.cglib;


/**
 * @author
 * @version 1.0.0
 * @ClassName Target
 * @Description
 * @createTime 2022/1/25  21:58:35
 */
public class Target   {

    public void save() {
        System.out.println("target is running...");
    }
}
