package SpringStudyDemo.proxy.cglib;

import SpringStudyDemo.proxy.jdk.TargetInterface;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author
 * @version 1.0.0
 * @ClassName ProxyTest
 * @Description  基于cglib的动态代理
 * @createTime 2022/1/26  14:20:53
 */
public class ProxyTest {
    public static void main(String[] args) {

        final Enhance enhance = new Enhance();
        final Target target = new Target();

        //1，创建增强器
        Enhancer enhancer = new Enhancer();

        //2,设置父类
        enhancer.setSuperclass(Target.class);

        //3，设置回调
        enhancer.setCallback((MethodInterceptor) (o, method, objects, methodProxy) -> {
            //执行前置
            enhance.before();
            //执行目标
            Object invoke = method.invoke(target, objects);
            //执行后置
            enhance.after();
            return invoke;
        });

        //4,创建代理对象
        Target t = (Target) enhancer.create();

        t.save();


    }
}
