package SpringStudyDemo.proxy.cglib;

/**
 * @author
 * @version 1.0.0
 * @ClassName Enhance
 * @Description
 * @createTime 2022/1/25  22:04:22
 */
public class Enhance {
    public void before(){
        System.out.println("前置增强");
    }

    public void after(){
        System.out.println("后置增强");
    }
}
