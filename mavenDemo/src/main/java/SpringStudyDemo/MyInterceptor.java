package SpringStudyDemo;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author
 * @version 1.0.0
 * @ClassName MyInterceptor
 * @Description
 * @createTime 2022/1/23  19:45:27
 */
public class MyInterceptor implements HandlerInterceptor {

    //在目标方法执行之前执行
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("在目标方法执行之前执行");
        /*String param = request.getParameter("param");
        if ("yes".equals(param)){
            return true;
        }else {
            request.getRequestDispatcher("/hello.jsp").forward(request,response);
            return false;//返回ture代表放行，false代表不放行
        }*/
        return true;
    }

    //在目标方法执行之后，视图对象返回之前执行
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        modelAndView.addObject("name", "改了");
        System.out.println("在目标方法执行之后执行");
    }

    //在整个流程执行完毕后执行
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("在流程完成之后执行");
    }
}
