package SpringStudyDemo;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserDaoImpl
 * @Description  dao层的注解配置
 * @createTime 2022/1/7  15:29:02
 */

//    <bean id="userDao" class="SpringStudyDemo.UserDaoImpl" scope="singleton" init-method="init" destroy-method="destroy"></bean>
//@Component("userDao")
@Repository("userDao")
public class UserDaoImpl implements UserDao{
    private List<String> stringList;
    private Map<String,UserTest> userMap;
    private Properties properties;
    private String username;
    private int age;

    public UserDaoImpl() {
        System.out.println("证明bean创建时调用的是无参构造器");
    }

    public void setStringList(List<String> stringList) {
        this.stringList = stringList;
    }

    public void setUserMap(Map<String, UserTest> userMap) {
        this.userMap = userMap;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAge(int age) {
        this.age = age;
    }
    @PostConstruct
    public void init(){
        System.out.println("初始化····");
    }
    @PreDestroy
    public void destroy(){
        System.out.println("销毁····");
    }

    @Override
    public void save() {
        System.out.println(stringList);
        System.out.println(userMap);
        System.out.println(properties);

        System.out.println(username+"=="+age+"==>"+"save running.......");
    }
}
