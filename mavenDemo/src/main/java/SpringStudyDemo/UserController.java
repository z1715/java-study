package SpringStudyDemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserController
 * @Description
 * @createTime 2022/1/7  21:16:07
 */
//@controller("")
public class UserController {
    public static void main(String[] args) {
        //ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        ApplicationContext app = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        UserService userService =(UserService) app.getBean("userService");
        userService.save();
    }
}
