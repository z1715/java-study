package SpringStudyDemo.aopAnnotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author
 * @version 1.0.0
 * @ClassName Enhance
 * @Description   需要在applicationContext.xml中配置扫描路径，其中已经有之前配置的
 * @createTime 2022/1/25  22:04:22
 */
@Component("myAspect")
@Aspect//标注当前类是一个切面
public class MyAspect {

    //抽取切点表达式（这里不知道哪里有问题反馈异常找不到切点，不用切点表达式就正常）
    @Pointcut("execution(* SpringStudyDemo.aopXML.*.*(..))")
    public void pointcut(){

    }
    @Before("pointcut()")
    public void before(){
        System.out.println("前置增强");
    }
    @AfterReturning("execution(public void SpringStudyDemo.aopAnnotation.Target.save())")
    public void afterReturn(){
        System.out.println("后置增强");
    }
    @Around("execution(public void SpringStudyDemo.aopAnnotation.Target.save())")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕前增强，，");
        //切点方法执行
        Object proceed = proceedingJoinPoint.proceed();
        System.out.println("环绕后增强，，");
        return proceed;
    }
    @AfterThrowing("execution(public void SpringStudyDemo.aopAnnotation.Target.save())")
    public void afterThrowing(){
        System.out.println("增强异常方法");
    }
    @After("execution(public void SpringStudyDemo.aopAnnotation.Target.save())")
    public void after(){
        System.out.println("这里的增强一定执行");
    }
}
