package SpringStudyDemo;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserService
 * @Description
 * @createTime 2022/1/7  18:12:08
 */
public interface UserService {
    void save();
}
