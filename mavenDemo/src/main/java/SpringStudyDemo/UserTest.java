package SpringStudyDemo;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserTest
 * @Description  一个测试类
 * @createTime 2022/1/9  15:30:23
 */
public class UserTest {
    private String username;
    private int age;

    public UserTest() {
    }

    public UserTest(String username, int age) {
        this.username = username;
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "UserTest{" +
                "username='" + username + '\'' +
                ", age=" + age +
                '}';
    }
}
