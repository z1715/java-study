package SpringStudyDemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @author
 * @version 1.0.0
 * @ClassName ContextLoaderListener
 * @Description
 * @createTime 2022/1/13  21:23:31
 */
@WebListener
public class ContextLoaderListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        String contextConfig = servletContext.getInitParameter("contextConfig");
        ApplicationContext app = new AnnotationConfigApplicationContext(contextConfig);
        //另一种写法
        //ApplicationContext app = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        //将Spring的应用对象存储到SpringContext域中

        servletContext.setAttribute("app",app);
        System.out.println("ServletContext创建完毕，，，，");

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("ServletContext销毁了，，，，");
    }
}
