package SpringStudyDemo.aopXML;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author
 * @version 1.0.0
 * @ClassName Enhance
 * @Description
 * @createTime 2022/1/25  22:04:22
 */
public class MyAspect {
    public void before(){
        System.out.println("前置增强");
    }

    public void afterReturn(){
        System.out.println("后置增强");
    }

    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕前增强，，");
        //切点方法执行
        Object proceed = proceedingJoinPoint.proceed();
        System.out.println("环绕后增强，，");
        return proceed;
    }
    public void afterThrowing(){
        System.out.println("增强异常方法");
    }

    public void after(){
        System.out.println("这里的增强一定执行");
    }
}
