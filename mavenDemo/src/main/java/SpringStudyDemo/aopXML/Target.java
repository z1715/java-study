package SpringStudyDemo.aopXML;

/**
 * @author
 * @version 1.0.0
 * @ClassName Target
 * @Description
 * @createTime 2022/1/25  21:58:35
 */
public class Target implements TargetInterface {
    @Override
    public void save() {
        /*int i =1/0;*/
        System.out.println("target is running...");
    }
}
