package SpringStudyDemo.aopXML;

/**
 * @author
 * @version 1.0.0
 * @ClassName TargetInterface
 * @Description
 * @createTime 2022/1/25  21:55:00
 */
public interface TargetInterface {
    void save();
}
