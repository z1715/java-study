package SpringStudyDemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserServiceImpl
 * @Description service层的注解
 * @createTime 2022/1/7  18:14:14
 */

//    <bean id="userService" class="SpringStudyDemo.UserServiceImpl">
//@Component("userService")
@Service("userService")
//@Scope("prototype")
public class UserServiceImpl implements UserService{
    @Value("${jdbc.driver}")
    private String driver;
    //        <constructor-arg name="use1rDao" ref="userDao"></constructor-arg>
    //按照数据类型从Spring容器中进行匹配
    //@Autowired
    //按照id值从容器中进行匹配，一般A和Q注解是一起使用
    //@Qualifier("userDao")
    //R注解=A注解+Q注解
    @Resource(name = "userDao")
    private UserDao userDao;

    public UserServiceImpl() {
    }

    public UserServiceImpl(UserDao use1rDao) {
        this.userDao = use1rDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void save() {
        /*ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao user =(UserDao) app.getBean("userDao");
        user.save();
        ((ClassPathXmlApplicationContext)app).close();
        System.out.println(user);*/
        System.out.println(driver);
        userDao.save();
    }

    //测试SpringMVC的异常处理器
    public void show(){
        Object a = "ss";
        Integer s = (Integer)a;
    }
}
