package SpringStudyDemo;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserDao
 * @Description
 * @createTime 2022/1/7  15:28:02
 */
public interface UserDao {
     void save();
}
