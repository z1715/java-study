package SpringStudyDemo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import pojo.User;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserController1
 * @Description  练习SpringMVC的数据请求与响应
 * @createTime 2022/1/15  17:45:23
 */
@Controller
@RequestMapping("/user")
public class UserController1 {

    @Resource
    private UserServiceImpl usi;

    @RequestMapping (value="/quick",method = RequestMethod.GET,params = {"username"})
    public String save(){
        System.out.println("controller 启动了");
        //return "hello.jsp";
        //加/代表从当前web应用目录（webapp）下找文件,前面默认有“forward:",如果想用重定向，就用”redirect:"
        return "/hello";
    }

    @RequestMapping (value="/quick2")
    public ModelAndView save2(){
        System.out.println("qucik2 is running...");
        usi.save();

        /*
        分为Model和View
        */
        ModelAndView modelAndView = new ModelAndView();
        //设置视图文件名称
        modelAndView.setViewName("/success");
        //设置模型名称
        modelAndView.addObject("username","jack");
        return modelAndView;
    }

    @RequestMapping (value="/quick3")
    public ModelAndView save3(ModelAndView modelAndView){

        //设置视图文件名称
        modelAndView.setViewName("/success");

        //设置模型名称
        modelAndView.addObject("username","直接传对象");
        return modelAndView;
    }

    @RequestMapping (value="/quick4")
    public String  save4(Model model){
        //设置模型名称
        model.addAttribute("username","quick3的另一种写法");
        return "/success";
    }
    @RequestMapping (value="/quick5")
    public String  save5(HttpServletRequest httpServletRequest){
        httpServletRequest.setAttribute("username","HttpServletRequest方式");
        return "/success";
    }
    //这个结果还是问号
    @RequestMapping (value="/quick6",produces = {"text/html;charset=utf-8"})
    public void   save6(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.getWriter().write("这是响应");

    }
    //这个结果是中文
    @RequestMapping (value="/quick7",produces = {"text/html;charset=utf-8"})
    @ResponseBody
    public String  save7( ){
        return "这是单纯返回字符串";
    }
    //text后面的json不用会中文乱码，用html好像也不乱，但字的格式会变
    @RequestMapping (value="/quick8",produces = {"text/json;charset=utf-8"})
    @ResponseBody
    public String  save8( ) throws JsonProcessingException {
        User user =new User();
        user.setUsername("李四");
        user.setPassword("1234");
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(user);
        return json;
    }
    //通过配置让SpringMVC自动把返回的对象转换成json字符串，这种方式能自动解决中文乱码问题
    @RequestMapping("/quick9")
    @ResponseBody
    public User save9(){
        User user =new User();
        user.setUsername("乐乐");
        user.setPassword("1234");
        return user;
    }

    @RequestMapping("/quick10")
    @ResponseBody
    public void save10(String username,int age){
        System.out.println(username);
        System.out.println(age);
    }

    //只要属性能对上，mvc可以自动把数据封装成对象
    @RequestMapping("/quick11")
    @ResponseBody
    public void save11(User user){
        System.out.println(user);
    }

    @RequestMapping("/quick12")
    @ResponseBody
    public void save12(String[] str ){
        System.out.println(Arrays.asList(str));
    }

    @RequestMapping("/quick13")
    @ResponseBody
    public void save13(@RequestParam("name") String username){
        System.out.println(username);
    }

    //Restful风格
    @RequestMapping("/quick14/{name}")
    @ResponseBody
    public void save14(@PathVariable("name") String username){
        System.out.println(username);
    }

    //通过这个对比可以看出MVC框架是否自动帮你注入数据
    @RequestMapping("/quick15")
    @ResponseBody
    public void save15(HttpServletRequest request, HttpServletResponse response, HttpSession session,User user){
        System.out.println(request);
        System.out.println(response);
        System.out.println(session);
        System.out.println(user);
        /*控制台结果：
        org.apache.catalina.connector.RequestFacade@4b06ea58
        org.apache.catalina.connector.ResponseFacade@18134e65
        org.apache.catalina.session.StandardSessionFacade@50cef075
        User{id=null, username='null', password='null', gender='null', addr='null'}*/
    }

    //这个请求头注解后面的参数似乎不在乎大小写
    @RequestMapping("/quick16")
    @ResponseBody
    public void save16(@RequestHeader("user-agent") String username){
        System.out.println(username);
    }

    //这个CookieValue注解后面的参数区分大小写
    @RequestMapping("/quick17")
    @ResponseBody
    public void save17(@CookieValue("JSESSIONID") String username){
        System.out.println(username);
    }

    @RequestMapping("/quick18")
    @ResponseBody
    public void save18(String name, MultipartFile uploadFile) throws IOException {
        String filename = uploadFile.getOriginalFilename();
        System.out.println(name);
        uploadFile.transferTo(new File("E:\\test\\"+filename));
    }
}
