package SpringStudyDemo;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

/**
 * @author
 * @version 1.0.0
 * @ClassName SpringConfiguration
 * @Description  Spring框架的注解方式配置文件
 * @createTime 2022/1/7  10:11:24
 */
//@Configuration
//配置扫描包的路径。=    <context:component-scan base-package="SpringStudyDemo"/>
@ComponentScan("SpringStudyDemo")
//加载多个配置文件就是{xx.class,xx.class}
@Import(DataSource.class)
public class SpringConfiguration {
    public SpringConfiguration() {
        System.out.println("注解配置文件启动了。。。。");
    }
}
