import pojo.User;
import mapper.UserMaer;
//import mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

public class MybatisTest1 {
    public static void main(String[] args) throws Exception {
        //加载mybatis的核心配置文件，获取SqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //获取SqlSessionc对象，用他来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //执行sql
       // List<User> users = sqlSession.selectList("mybatis.selectAll");
        //获取UserMapper的接口代理对象
        UserMaer mapper = sqlSession.getMapper(UserMaer.class);
        List<User> users = mapper.selectAll();
        List<User> users1 = mapper.selectOne();


        System.out.println(users);
        System.out.println(users1);
        //释放资源
        sqlSession.close();


    }
}
