import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author
 * @version 1.0.0
 * @ClassName ResponseDemo
 * @Description  重定向
 * @createTime 2021/12/20  15:54:38
 */
@WebServlet("/resp")
public class ResponseDemo extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("resp`````");
       /* //1,先设置状态码,302
        resp.setStatus(302);
        //2,设置响应头，location
        resp.setHeader("location","/MavenTomcatWeb/resp1");*/
        //动态获取虚拟目录
        String contextPath = req.getContextPath();
        //简化方式完成重定向，效果和上面一样
        resp.sendRedirect(contextPath+"/resp1");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
