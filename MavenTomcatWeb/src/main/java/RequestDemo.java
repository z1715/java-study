import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * @author
 * @version 1.0.0
 * @ClassName ServletDemo1
 * @Description
 * @createTime 2021/12/17  19:05:58
 */
@WebServlet("/demo3")
public class RequestDemo extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //解决get方式数据中的中文乱码问题
        String parameter = req.getParameter("username");
        //编码格式也可以用这种方式表示。StandardCharsets.ISO_8859_1
        byte[] bytes = parameter.getBytes("ISO-8859-1");
        String s = new String(bytes, "UTF-8");
        System.out.println("解决乱码前的中文：" + parameter);
        System.out.println("解决乱码后的中文：" + s);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //解决post方式的中文乱码问题
        req.setCharacterEncoding("UTF-8");

        String parameter = req.getParameter("文本");
        System.out.println(parameter);
    }
}
