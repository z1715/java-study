import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author
 * @version 1.0.0
 * @ClassName RequestDemo1
 * @Description 避免get请求中有中文出现乱码
 * @createTime 2021/12/20  11:02:51
 */
@WebServlet("/req2")
public class RequestDemo2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("req2.......");
        String username = req.getParameter("username");
        byte[] usernames = username.getBytes("ISO-8859-1");
        String s = new String(usernames, StandardCharsets.UTF_8);
        System.out.println(s);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }
}
