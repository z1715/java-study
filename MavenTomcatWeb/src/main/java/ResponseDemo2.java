import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author
 * @version 1.0.0
 * @ClassName ResponseDemo2
 * @Description response字节输出流
 * @createTime 2021/12/20  20:03:21
 */
@WebServlet("/resp2")
public class ResponseDemo2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//1,读取文件
        FileInputStream fis = new FileInputStream("E:\\gg.png");
        //2,获取字节输出流
        ServletOutputStream outputStream = resp.getOutputStream();
        /*//3，完成流的复制
        byte[] bytes = new byte[1024];
        int len = 0;
        while ((len = fis.read(bytes)) != -1){
            outputStream.write(bytes,0,len);
        }*/
        //使用工具类完成第3步
        IOUtils.copy(fis,outputStream);
        fis.close();

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }
}
