package com.daozheng.run;

import com.daozheng.bean.Business;
import com.daozheng.bean.Customs;
import com.daozheng.bean.Movie;
import com.daozheng.bean.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MovieSystem {
    /*1，需要定义集合来存储商家和用户的数据
        又因为只需要一个集合，所以把该集合定义成静态的
        里面即存储用户又存储商家，所以集合类型为父类User*/
    public static final List<User> ALL_USERS = new ArrayList<>();
    /*2,存储所有商家的排片信息，用map存，影片信息经常需要变动，适合用数组*/
    public static Map<Business, List<Movie>> All_MOVIES = new HashMap<>();

    /*3,准备一些测试数据*/
    static {
        Customs c = new Customs();
        c.setLoginName("cxt");
        c.setPassWord("23432432");
        c.setUserName("陈世美");
        c.setGender('男');
        c.setPhone(55667);
        c.setBalance(444.4);
        ALL_USERS.add(c);

        Customs c1 = new Customs();
        c1.setLoginName("gds");
        c1.setPassWord("23116672");
        c1.setUserName("郭达森");
        c1.setGender('男');
        c1.setPhone(55667);
        c1.setBalance(444.4);
        ALL_USERS.add(c1);

        Customs c2 = new Customs();
        c2.setLoginName("wf33");
        c2.setPassWord("23432782");
        c2.setUserName("王菲");
        c2.setGender('女');
        c2.setPhone(58899);
        c2.setBalance(444.4);
        ALL_USERS.add(c2);

        Business b = new Business();
        b.setLoginName("doctor");
        b.setPassWord("23432782");
        b.setUserName("博士");
        b.setGender('女');
        b.setPhone(58899);
        b.setBalance(444.4);
        b.setAddress("翻斗乐园影视");
        b.setShopName("东云研究所");
        ALL_USERS.add(b);

        List<Movie> movies = new ArrayList<>();//这里是商家1的信息
        All_MOVIES.put(b, movies);

        Business b1 = new Business();
        b1.setLoginName("maliya");
        b1.setPassWord("1234");
        b1.setUserName("耀光");
        b1.setGender('女');
        b1.setPhone(58899);
        b1.setBalance(444.4);
        b1.setAddress("卡西米尔");
        b1.setShopName("罗德岛");
        ALL_USERS.add(b1);

        List<Movie> movies1 = new ArrayList<>();//这里是商家2的信息
        All_MOVIES.put(b1, movies1);

        Business b2 = new Business();
        b2.setLoginName("talula");
        b2.setPassWord("23432782");
        b2.setUserName("塔露拉");
        b2.setGender('女');
        b2.setPhone(58899);
        b2.setBalance(444.4);
        b2.setAddress("乌萨斯");
        b2.setShopName("整合运动");
        ALL_USERS.add(b2);

        List<Movie> movies2 = new ArrayList<>();//这里是商家3的信息
        All_MOVIES.put(b2, movies2);


    }

    public static final Scanner SC = new Scanner(System.in);

    public static User loginUser;
    //设置时间格式
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    //设置日志
    public static final Logger LOGGER = LoggerFactory.getLogger("MovieSystem.class");
    public static void main(String[] args) {
        showMain();
    }

    /*首页展示*/
    private static void showMain() {

        while (true) {
            System.out.println("================电影首页=======================");
            System.out.println("1，登录");
            System.out.println("2，用户注册");
            System.out.println("3，商家注册");
            System.out.println("请输入操作命令");
            String command = SC.nextLine();
            switch (command) {
                case "1":
                    login();
                    break;
                case "2":
                    break;
                case "3":
                    break;
                default:
                    System.out.println("请输入正确的命令");
            }
        }
    }

    /*登录功能*/
    private static void login() {
        System.out.println("请输入登录名称：");
        String loginName = SC.nextLine();
        System.out.println("请输入登录密码：");
        String password = SC.nextLine();

        User user = getUserByLoginName(loginName);
        if (user != null) {
            if (user.getPassWord().equals(password)) {
                //登录成功
                loginUser = user;
                if (user instanceof Customs) {
                    //是客户
                    showCustoms();
                } else {
                    //是商家
                    showBusiness();
                }
                return;
            } else {
                System.out.println("密码错误");
            }
        } else {
            System.out.println("登录名称错误");
        }
    }

    /*商家首页*/
    private static void showBusiness() {
        System.out.println("================商家后台=======================");
        System.out.println("欢迎" + loginUser.getUserName() + (loginUser.getGender()=='男'?"先生":"女士"));
        System.out.println("1，所有电影");
        System.out.println("2，上架电影");
        System.out.println("3，下架电影");
        System.out.println("4，修改电影");
        System.out.println("5，退出");
        System.out.println("请输入操作命令：");
        String comand = SC.nextLine();
        switch (comand) {
            case "1":
                queryAll();
                break;
            case "2":
                addMovie();
                break;
            case "3":
                deleteMovie();
                break;
            case "4":
                modifyMovie();
                break;
            case "5":
                System.out.println("已成功退出系统");
                return;
            default:
                System.out.println("请输入正确的命令");
                break;
        }

    }


    /*当前商家-修改影片*/
    private static void modifyMovie() {
    }
    /*当前商家-下架影片*/
    private static void deleteMovie() {
        System.out.println("==========下架电影==============");
        Business b = (Business)loginUser;
        List<Movie> movieList = All_MOVIES.get(b);

        if (movieList.size()>0 ) {
            System.out.println("请输入要删除的影片的名称");
            String name = SC.nextLine();
            Movie m = getMovieByName(name);
            if (m!=null){
                movieList.remove(m);
                System.out.println(m.getName() + "已成功下架");
                return;
            }else {
                System.out.println("未找到该影片");
                return;
            }
        } else {
            System.out.println("当前无影片");
            return;
        }

    }

    private static Movie getMovieByName(String name) {
        Business b = (Business)loginUser;
        List<Movie> movieList = All_MOVIES.get(b);
        for (Movie movie:movieList) {
            if (movie.getName().contains(name)){
                return movie;
            }
        }
        return null;
    }

    /*当前商家-添加影片*/
    private static void addMovie() {
        System.out.println("==========上架电影==============");
        Business b = (Business)loginUser;
        List<Movie> movieList = All_MOVIES.get(b);


        System.out.println("请输入片名");
        String name = SC.nextLine();
        System.out.println("请输入主演");
        String actor = SC.nextLine();
        System.out.println("请输入时长");
        String time = SC.nextLine();
        System.out.println("请输入价格");
        String price = SC.nextLine();
        System.out.println("请输入余票");
        String theRemainingTickets = SC.nextLine();
        Movie m = null;
        while (true) {
            try {
                System.out.println("请输入放映时间");
                String theLengthOfTheMovie = SC.nextLine();

                m = new Movie();
                m.setName(name);
                m.setActor(actor);
                m.setPrice(Double.valueOf(price));
                m.setTheLengthOfTheMovie(sdf.parse(theLengthOfTheMovie));
                m.setTime(Double.valueOf(time));
                m.setTheRemainingTickets(Integer.valueOf(theRemainingTickets));
                movieList.add(m);
                System.out.println("影片已成功上架");
                All_MOVIES.put(b,movieList);
                return;
            } catch (ParseException e) {
                e.printStackTrace();
                LOGGER.error("时间解析有问题" + e);
            }
        }





    }

    /*当前商家-查询所有影片信息*/
    private static void queryAll() {
        System.out.println("==========查询电影信息==============");
        LOGGER.info(loginUser.getUserName() + "进入了后台");
        List<Movie> movieList = All_MOVIES.get(loginUser);
        if (movieList.size() > 0) {
            for(Movie m:movieList){

                System.out.println("电影名称：" + m.getName() + "\t主演" + m.getActor() +
                        "\t评分" + m.getScore() + "\t时长" + m.getTime() + "\t价格" + m.getPrice());
            }
        } else {
            System.out.println("影片库当前没有片");
        }
    }

    /*用户首页*/
    private static void showCustoms() {
        System.out.println("================用户首页=======================");
        System.out.println("1，展示所有电影");
        System.out.println("2，查询电影");
        System.out.println("3，电影评分");
        System.out.println("4，购票");
        System.out.println("5，退出");
        System.out.println("请输入操作命令：");
        String comand = SC.nextLine();
        switch (comand) {
            case "1":
                queryAllMovie();
                break;
            case "2":
                queryByName();
                break;
            case "3":
                score();
                break;
            case "4":

                break;
            case "5":
                System.out.println("已成功退出系统");
                return;
            default:
                System.out.println("请输入正确的命令");
                break;
        }

    }
    //显示所有商家的所有电影
    private static void queryAllMovie() {
        All_MOVIES.forEach((business, movies) -> {
            System.out.println("==========当前电影信息==============");
            List<Movie> movieList = All_MOVIES.get(business);
            System.out.println(business.getShopName() + "\t\t" +business.getAddress());
            if (movieList.size() > 0) {
                for(Movie m:movieList){

                    System.out.println("电影名称：" + m.getName() + "\t主演" + m.getActor() +
                            "\t评分" + m.getScore() + "\t时长" + m.getTime() + "\t价格" + m.getPrice());
                }
            } else {
                System.out.println("影片库当前没有片");
            }
        });
    }

    /*根据名称查电影*/
    private static void queryByName() {
    }

    /*评分*/
    private static void score() {
    }

    public static User getUserByLoginName(String loginName) {
        for (User user : ALL_USERS) {
            if (user.getLoginName().equals(loginName)) {
                return user;
            }
        }
        return null;
    }

}
