package com.daozheng.bean;

import java.util.Date;

public class Movie {
    private String name;
    private String actor;
    private double score;
    private double time;
    private double price;
    private int theRemainingTickets;
    private Date theLengthOfTheMovie;

    public Movie() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public int getTheRemainingTickets() {
        return theRemainingTickets;
    }

    public void setTheRemainingTickets(int theRemainingTickets) {
        this.theRemainingTickets = theRemainingTickets;
    }

    public Date getTheLengthOfTheMovie() {
        return theLengthOfTheMovie;
    }

    public void setTheLengthOfTheMovie(Date theLengthOfTheMovie) {
        this.theLengthOfTheMovie = theLengthOfTheMovie;
    }
}
