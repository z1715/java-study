package com.daozheng.doit.mapper;

import com.daozheng.doit.pojo.Order;

/**
 * @author
 * @version 1.0.0
 * @ClassName OrderMapper
 * @Description
 * @createTime 2022/2/6  19:40:06
 */
public interface OrderMapper {
    void add(Order order);
}
