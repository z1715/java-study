package com.daozheng.doit.mapper;

import com.daozheng.doit.pojo.User;

import java.util.List;

/**
 * @author
 * @version 1.0.0
 * @ClassName UserMapper
 * @Description
 * @createTime 2022/1/30  21:26:18
 */
public interface UserMapper {
    List<User> selectAll();

    void save(User user);

    User selectById(int id);

}
