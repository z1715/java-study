package com.daozheng.doit.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * @author
 * @version 1.0.0
 * @ClassName DataTypeHandler
 * @Description
 * @createTime 2022/2/3  12:39:15
 */
public class DateTypeHandler extends BaseTypeHandler<Date> {
    //把java类型转换成数据库需要的类型
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, Date date, JdbcType jdbcType) throws SQLException {
        long time = date.getTime();
        preparedStatement.setLong(i,time);
    }

    //把数据库中的类型转换成java类型
    //String参数是要转换的字段名称
    //ResultSet是查询出的结果集
    @Override
    public Date getNullableResult(ResultSet resultSet, String s) throws SQLException {
        //获得结果集中需要的数据类型（long）并转换成Date类型
        long aLong = resultSet.getLong(s);
        Date date = new Date(aLong);
        return date;
    }
    //把数据库中的类型转换成java类型
    @Override
    public Date getNullableResult(ResultSet resultSet, int i) throws SQLException {
        long aLong = resultSet.getLong(i);
        Date date = new Date(aLong);
        return date;
    }
    //把数据库中的类型转换成java类型
    @Override
    public Date getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        long aLong = callableStatement.getLong(i);
        Date date = new Date(aLong);
        return date;
    }
}
