package com.daozheng.run;

import com.daozheng.proxy.ProxyUtil;
import com.daozheng.proxy.UserService;
import com.daozheng.proxy.UserServiceImpl;
import com.daozheng.reflect.ReflectObjectDemo;
import com.daozheng.reflect.Student;

public class RunTest {
    public static void main(String[] args) {
        //测试反射得到任意类的属性并写入文件
        Student s = new Student("自在",23);
        ReflectObjectDemo.reflectObject(s);
        //测试动态代理
        UserService userService = ProxyUtil.getProxyPlus(new UserServiceImpl());
        //直接new对应接口实现代理
        UserService userService1 = ProxyUtil.getProxyPlus(new UserService() {
            @Override
            public String login(String loginName, int password) {
                return "直接用接口的方式代理，不写实现类";
            }

            @Override
            public void selectAllUsers() {
                System.out.println("这种方式貌似省事");
            }

            @Override
            public boolean deleteAllUsers() {
                System.out.println("适合什么样的场景呢");
                return false;
            }
        });
        System.out.println(userService.login("admin",1234));
        System.out.println("===========================");
        System.out.println(userService.deleteAllUsers());
        System.out.println("===========================");
        userService.selectAllUsers();
        System.out.println("===========================");
        System.out.println(userService1.login("admin",1234));
        System.out.println("===========================");
        System.out.println(userService1.deleteAllUsers());
        System.out.println("===========================");
        userService1.selectAllUsers();

    }
}
