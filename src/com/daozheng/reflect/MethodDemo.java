package com.daozheng.reflect;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class MethodDemo {
    @Test
    //下面是获取所有方法名称及返回值类型
    public void methodTestAll() {
        Class s = Student.class;
        for (Method declaredMethod : s.getDeclaredMethods()) {
            System.out.println(declaredMethod.getName() + "==>" + declaredMethod.getReturnType());
        }
    }
    @Test
    //根据方法名称和返回值获取指定方法,并使用
    public void methodTest() {
        Class s = Student.class;
        try {
            //创建对象才能用方法
            Constructor c = s.getDeclaredConstructor();
            Student st = (Student) c.newInstance();

            //获取指定方法
            Method ws = s.getDeclaredMethod("writeStudent",String.class);
            Method ws1 = s.getDeclaredMethod("eatStudent");
            String text = "唱尽人情冷暖世情如霜";

            //打开权限
            ws1.setAccessible(true);
            //调用方法
            System.out.println(ws.invoke(st, text));
            ws1.invoke(st);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



}
