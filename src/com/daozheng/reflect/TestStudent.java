package com.daozheng.reflect;

import org.junit.Test;

import java.lang.reflect.Constructor;

import static org.junit.Assert.*;

public class TestStudent {
    @Test
    //得到全部构造器
    public void getStudenDeclaredtConstructors(){
        //1,获取类对象
        Class c = Student.class;
        //2,调用对象的getDeclaredConstructors方法
        Constructor[] declaredConstructors = c.getDeclaredConstructors();
        //3,遍历构造器
        for (Constructor declaredConstructor : declaredConstructors) {
            System.out.println(declaredConstructor.getName() + "============"
                    + declaredConstructor.getParameterCount() );
        }
    }

    @Test
    //得到公开构造器(指用public修饰的）
    public void getStudentConstructors(){
        //1,获取类对象
        Class c = Student.class;
        //2,调用对象的getConstructors方法
        Constructor[] Constructors = c.getConstructors();
        //3,遍历构造器
        for (Constructor Constructor : Constructors) {
            System.out.println(Constructor.getName() + "============"
            + Constructor.getParameterCount() );
        }
    }

    @Test
    //得到单个公开构造器
    public void getStudentConstructor(){
        //1,获取类对象
        Class c = Student.class;
        //2,调用对象的getConstructor方法，参数列表是可选参数，参数类型例如：String.class、int.class等
        Constructor Constructor = null;
        try {
            Constructor = c.getConstructor(String.class,int.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        //3,
            System.out.println(Constructor.getName() + "============"
                    + Constructor.getParameterCount() );
        }

    @Test
    //得到单个构造器
    public void getStudentDeclaredConstructor(){
        //1,获取类对象
        Class c = Student.class;
        //2,调用对象的getConstructor方法，参数列表是可选参数，参数类型例如：String.class、int.class等
        Constructor Constructor = null;
        try {
            Constructor = c.getConstructor(String.class,int.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        //3,
        System.out.println(Constructor.getName() + "============"
                + Constructor.getParameterCount() );
    }
}