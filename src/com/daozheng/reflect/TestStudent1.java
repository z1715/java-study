package com.daozheng.reflect;

import org.junit.Test;

import java.lang.reflect.Constructor;

public class TestStudent1 {


    @Test
    //得到单个构造器
    public void getStudentDeclaredConstructor() {
        //1,获取类对象
        Class c = Student.class;
        //2,调用对象的getConstructor方法，参数列表是可选参数，参数类型例如：String.class、int.class等
        try {
            Constructor constructor = c.getDeclaredConstructor(String.class, int.class);
            //3,反射的方式创建对象
           // Student s = (Student) constructor.newInstance("黄秋生", 33);
            //4,如果构造器是私有的，那么可以用setAccessible来强行在当下使用（非永久）
            constructor.setAccessible(true);
            Student s = (Student) constructor.newInstance("黄秋生", 33);
            //下面这行证明解开限制后在当前块可以反复使用
            Student s1 = (Student) constructor.newInstance("黄秋", 33);

            System.out.println(constructor.getName() + "============"
                    + constructor.getParameterCount());
            System.out.println(s);
            System.out.println(s1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}