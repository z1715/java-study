package com.daozheng.reflect;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class FieldDemo {
    @Test
    //得到类中的所有成员变量
    public void getDeclaredFieldsDemo(){
        Class s = Student.class;

        for (Field declaredField : s.getDeclaredFields()) {
            System.out.println(declaredField.getName() +"==>" + declaredField.getType());
        }
    }

    @Test
    //根据成员变量名称得到类中指定的成员变量，并赋值
    public void getDeclaredFieldDemo(){
        Class s = Student.class;
        try {
            Field f =s.getDeclaredField("name");
            System.out.println(f.getName()+"==>" + f.getType());

            //打开权限
            f.setAccessible(true);

            Constructor s1 = s.getConstructor();
            Student s2 = (Student) s1.newInstance();

            //赋值
            f.set(s2,"月月");
            System.out.println(s2);

            //取值
            String a = (String) f.get(s2);
            System.out.println(a);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
