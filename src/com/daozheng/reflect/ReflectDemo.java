package com.daozheng.reflect;

import java.lang.reflect.Method;
import java.util.ArrayList;

/*得到class文件的3种方式
* 测试向整形集合加字符串*/
public class ReflectDemo {
    public static void main(String[] args) {
        try {
            //1，静态方法forname中的参数是全限定类名（包名+类名），得到class文件
            Class c = Class.forName("com.daozheng.reflect.Student");
            System.out.println(c);

            //2,类名.class
            Class c1 = Student.class;
            System.out.println(c1);

            //3,对象名.getClass()
            Student s = new Student();
            Class c3 = s.getClass();
            System.out.println(c3);

            //测试向整数集合加字符串
            ArrayList<Integer> tar = new ArrayList<>();
            reflectList(tar, "悟空");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void reflectList(ArrayList arrayList, String text){
        try {
            Class list = arrayList.getClass();
            Method add = list.getDeclaredMethod("add", Object.class);
            add.invoke(arrayList,text);
            System.out.println(arrayList);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
