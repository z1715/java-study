package com.daozheng.reflect;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectObjectDemo {
    public static void reflectObject(Object object) {
        try (PrintStream ps = new PrintStream(
                new FileOutputStream("E:\\JS\\js\\b\\test.txt",true),true)){
        Class c = object.getClass();
        //getSimpleName获取类名，getName获取全限定类名
            ps.println("=============" + c.getName() + "=============");
            ps.println("=============" + c.getSimpleName() + "=============");
            for (Field field : c.getDeclaredFields()) {
                field.setAccessible(true);
                String n = field.getName();
                String v = field.get(object) + "";
                String t = field.getType() + "";


                ps.println("变量类型是：" + t + "，变量名称是"+n + "，值是" + v);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
