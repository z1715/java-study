package com.daozheng.threadpool;

public class TestRunnable implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 2; i++) {
            System.out.println(Thread.currentThread().getName() + "执行了" + i );
        }
        try {
            System.out.println(Thread.currentThread().getName() + "马上进入休眠"  );
            Thread.sleep(100000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
