package com.daozheng.threadpool;

import java.util.concurrent.*;

/*线程池的使用*/
public class ThreadPoolDemo {
    public static void main(String[] args) {
        /*                        int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler*/
        ExecutorService pool = new ThreadPoolExecutor(3, 5,
                4, TimeUnit.SECONDS, new ArrayBlockingQueue<>(5),
                Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
/*
        //把任务交给线程池
        Runnable r = new TestRunnable();
        pool.execute(r);
        pool.execute(r);
        pool.execute(r);

        //现在不会创建临时线程
        pool.execute(r);
        pool.execute(r);
        pool.execute(r);
        pool.execute(r);
        pool.execute(r);

        //现在创建临时线程了
        pool.execute(r);
        pool.execute(r);

        //不创建线程了，拒绝策略被触发
       // pool.execute(r);

        //关闭线程池（实际开发一般不使用）,立即关闭任务，即使任务没有完成，会丢失任务
        //pool.shutdownNow();

        pool.shutdown();//等全部线程执行完再关（即在等的，临时的，核心的）*/
        Future<String> f = pool.submit(new MyCallable(1000));
        Future<String> f1 = pool.submit(new MyCallable(2000));
        Future<String> f2 = pool.submit(new MyCallable(3000));
        Future<String> f3 = pool.submit(new MyCallable(4000));
        try {
            System.out.println(f.get());
            System.out.println(f1.get());
            System.out.println(f2.get());
            System.out.println(f3.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }


}

class MyCallable implements Callable<String>{

    private int n;

    public MyCallable() {
    }

    public MyCallable(int n) {
        this.n = n;
    }

    @Override
    public String call() throws Exception {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += i;
        }
        return Thread.currentThread().getName() + "执行1-" + n +"的结果是" + "===>" + sum + "";
    }
}
