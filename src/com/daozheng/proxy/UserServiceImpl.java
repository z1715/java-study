package com.daozheng.proxy;

public class UserServiceImpl implements UserService{
    @Override
    public String login(String loginName, int password) {
        if("admin".equals(loginName) && "1234".equals(String.valueOf(password))){
            return "登录成功";
        }else {
            return "请重新输入帐号密码";
        }

    }

    @Override
    public void selectAllUsers() {
        System.out.println("操作成功");
    }

    @Override
    public boolean deleteAllUsers() {
        System.out.println("删除操作成功");
        return false;
    }
}
