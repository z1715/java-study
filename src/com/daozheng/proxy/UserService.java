package com.daozheng.proxy;

public interface UserService {
     String login(String loginName,int password);
     void selectAllUsers();
     boolean deleteAllUsers();
}

