package com.daozheng.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/*创建动态代理方法
* public static Object newProxyInstance(ClassLoader loader,
                                          Class<?>[] interfaces,
                                          InvocationHandler h)
  参数一：类加载器，负责加载代理类到内在中使用
  参数二：获取被代理对象实现的全部接口。代理要为全部接口的全部方法进行代理
  参数三：代理的核心处理逻辑（就是需要代理做什么）*/
public class ProxyUtil {
    /*生成业务对象的代理对象（这里指定了UserService接口），下面有更进一步的写法*/
    public static UserService getProxy(UserService obj){
        //返回一个代理对象
        return (UserService) Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        /*参数一：代理对象本身，一般不管
                        * 参数二：正在被代理的方法
                        * 参数三：被代理方法应该传入的参数*/
                        System.out.println("代理对象：" +proxy.getClass().getName() +"\r\n" +
                                "被代理的方法：" + method.getName() +"\r\n" +
                                "被代理方法应该传入的参数： " + Arrays.toString(args));
                        //触发方法的真正执行，也就是被代理的方法
                        Object result = method.invoke(obj, args);
                        System.out.println("在这里可以添加想要的功能而不用再修改代码（比如计时）。");
                        return result;
                    }
                });
    }
    /*生成业务对象的代理对象，可用于任意接口*/
    public static <T> T getProxyPlus(T obj){
        //返回一个代理对象
        return (T) Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(),
                (proxy, method, args) -> {
                    /*参数一：代理对象本身，一般不管
                     * 参数二：正在被代理的方法
                     * 参数三：被代理方法应该传入的参数*/
                    System.out.println("代理对象：" +proxy.getClass().getName() +"\r\n" +
                            "被代理的方法：" + method.getName() +"\r\n" +
                            "被代理方法应该传入的参数： " + Arrays.toString(args));
                    //触发方法的真正执行，也就是被代理的方法
                    Object result = method.invoke(obj, args);
                    System.out.println("在这里可以添加想要的功能而不用再修改代码（比如计时）。");
                    return result;
                });
    }
}
