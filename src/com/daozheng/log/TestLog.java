package com.daozheng.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//搭建日志框架
public class TestLog {
    //创建logback日志对象
    public static Logger LOGGER =  LoggerFactory.getLogger("TestLog.class");

    public static void main(String[] args) {
        try {
            LOGGER.debug("方法开始执行");
            LOGGER.info("second");
            int a = 900;
            int b = 10;
            LOGGER.info("a="+a);
            LOGGER.trace("b="+b);
            System.out.println(a/b);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("干不了 "+e);
        }
    }
}
