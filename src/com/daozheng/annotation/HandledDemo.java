package com.daozheng.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*练习自定义注解和元注解*/
//注解能用在哪里（如方法、类、构造器、成员变量,想知道具体的去ElementType中看）
@Target({ElementType.METHOD,ElementType.FIELD})
//查看方式同上面
@Retention(RetentionPolicy.RUNTIME)
public @interface HandledDemo {
    String value();//值必须是value才能在只有value没有默认值的时候省略类型，不管自定义注解有多少属性。
    int number() default 1;
}
