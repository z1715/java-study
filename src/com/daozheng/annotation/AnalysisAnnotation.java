package com.daozheng.annotation;

import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;

/*注解解析*/
public class AnalysisAnnotation {
    @Test
    public  void analysis (){
        //1,获取类对象
        Class c = BookStore.class;
        //2，判断类上是否存在某个注解
        if(c.isAnnotationPresent(Book.class)){
            //3,直接获取注解,已知注解类型直接强转
            Book a =(Book) c.getDeclaredAnnotation(Book.class);
            //4,获取注解内的属性
            System.out.println(a.price());
            System.out.println(a.value());
            System.out.println(Arrays.toString(a.authors()));
        }
        //1,获取所有方法上的注解
        for (Method declaredMethod : c.getDeclaredMethods()) {
            Book b =declaredMethod.getDeclaredAnnotation(Book.class);
            System.out.println(b.price());
            System.out.println(b.value());
            System.out.println(Arrays.toString(b.authors()));

        }
    }
}
