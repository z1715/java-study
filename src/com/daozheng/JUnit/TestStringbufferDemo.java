package com.daozheng.JUnit;

import org.junit.*;

import static org.junit.Assert.*;

public class TestStringbufferDemo {
    @Before
    public void before(){
        System.out.println("这是Before,修饰实例方法，在每个测试方法执行前执行一次");
    }
    @After
    public void after(){
        System.out.println("这是After，修饰实例方法，在每个测试方法执行后执行一次");
    }
    @BeforeClass
    public static void beforeClass(){
        System.out.println("这是BeforeClass，修饰静态方法，在所有测试方法之前只执行一次");
    }
    @AfterClass
    public static void afterClass(){
        System.out.println("这是AfterClass，修饰静态方法，在所有测试方法之前只执行一次");
    }

    @Test
    public void testTest() {
        StringbufferDemo stringbufferDemo =new StringbufferDemo();
        String s = stringbufferDemo.test("测试类零啊");

        Assert.assertEquals("有的地方错了","构造器里放字符串会以该字符串开头：测试类零啊",s);
    }

    @Test
    public void testTest1() {
        StringbufferDemo stringbufferDemo =new StringbufferDemo();
        stringbufferDemo.test1("测试类一啊");
    }

    @Test
    public void testTest2() {
        StringbufferDemo stringbufferDemo =new StringbufferDemo();
        stringbufferDemo.test2("测试类二啊");
    }
}