package com.daozheng.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/*
* 学习Callable接口和FutureTask的使用*/
public class ThreadDemo5 {
    public static void main(String[] args)  {
        MyCallable m = new MyCallable(20);
        //FutureTask可以在线程执行完毕后调用其get方法得到线程执行完成的结果
        FutureTask<String> ft = new FutureTask<>(m);
        new Thread(ft).start();

        try {
            //FutureTask的get方法会等线程执行完毕再拿结果
            System.out.println(ft.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class MyCallable implements Callable<String>{

    private int n;

    public MyCallable() {
    }

    public MyCallable(int n) {
        this.n = n;
    }

    @Override
    public String call() throws Exception {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += i;
        }
        return Thread.currentThread().getName() + "===>" + sum + "";
    }
}
