package com.daozheng.thread;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ThreadDemo1 implements Runnable{

    private String url;//下载地址
    private String name;//下载后重命名

    public ThreadDemo1(String name, String url) {
        this.url = url;
        this.name = name;
    }


    @Override
    public void run() {
        WebDownloader wd = new WebDownloader();
        wd.downloader(url,name);
        System.out.println("成功下载了"+name);
    }

    public static void main(String[] args) {
        ThreadDemo1 td = new ThreadDemo1("1.jpg","https://wx1.sinaimg.cn/mw2000/002PnnDnly1guolj4ov35j62c02c0u0x02.jpg");
        ThreadDemo1 td1 = new ThreadDemo1("2.jpg","https://wx1.sinaimg.cn/mw2000/002PnnDnly1guolj4ov35j62c02c0u0x02.jpg");
        ThreadDemo1 td2 = new ThreadDemo1("3.jpg","https://wx1.sinaimg.cn/mw2000/002PnnDnly1guolj4ov35j62c02c0u0x02.jpg");

        //实际执行不是按照顺序从上到下执行
        new Thread(td).start();
        new Thread(td1).start();
        new Thread(td2).start();

        /*如果是用Thread类来做，首先继承Thread，其它不变
        * 把上面 三行换成
        * td.start();
        * td2.start();
        * td3.start();*/
    }
}

class WebDownloader{
    public void downloader(String url,String name)  {
        try {
            FileUtils.copyURLToFile(new URL(url),new File(name));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
