package com.daozheng.thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
/*用lock的方式解决同步
* 1，先创建一个ReentrantLock对象，最好用final修饰
* 2，然后用lock方法和unlock方法把需要同步的部分包起来
* 其中unlock最好放到finally中*/
public class ThreadDemo7 implements Runnable{
    private final Lock lock = new ReentrantLock();
    private int ticket = 100;
    @Override
    public void run() {
        getTicket();

    }
    public void getTicket(){
        while (true) {

            if (ticket<=0){
                break;
            }

            lock.lock();
            int i;
            try {
                i =ticket--;
            }finally {
                lock.unlock();
            }
            System.out.println(Thread.currentThread().getName() + "得到了第" + i + "张票");

        }
    }
    public static void main(String[] args) {
        ThreadDemo7 td = new ThreadDemo7();

        new Thread(td,"鲤鱼").start();
        new Thread(td,"千砂都").start();
        new Thread(td,"香音").start();
    }
}
