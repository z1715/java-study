package com.daozheng.thread;

public class ThreadDemo2 implements Runnable{
    @Override
    public void run() {
        //run()方法线程体
        for (int i = 0; i < 200; i++) {
            System.out.println("====在看视频"+i);
        }
    }

    public static void main(String[] args) {
        //main线程、主线程体
        //创建一个线程对象
        ThreadDemo2 t1 = new ThreadDemo2();
        //new一个Thread对象并把线程对象给它，再调用start（）
        new Thread(t1).start();


        for (int i = 0; i < 1000; i++) {
            System.out.println("在看代码----"+i);
        }
    }
}
