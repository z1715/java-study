package com.daozheng.thread;

//同时并发访问可能出问题，不过目前try/catch在if上不会出现问题，在if下会出现并发问题
public class ThreadDemo3 implements Runnable{

    private int ticket = 10;
    @Override
    public void run() {
        while (true) {

            if (ticket<=0){
                break;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "得到了第" + ticket-- + "张票");
        }

    }

    public static void main(String[] args) {
        ThreadDemo3 td = new ThreadDemo3();

        new Thread(td,"鲤鱼").start();
        new Thread(td,"千砂都").start();
        new Thread(td,"香音").start();
    }
}
