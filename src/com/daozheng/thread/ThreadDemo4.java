package com.daozheng.thread;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.*;


//callable方式实现多线程
public class ThreadDemo4 implements Callable<Boolean> {
    private String url;//下载地址
    private String name;//下载后重命名

    public ThreadDemo4(String name, String url) {
        this.url = url;
        this.name = name;
    }


    @Override
    public Boolean call() {
        WebDownloader wd = new WebDownloader();
        wd.downloader(url,name);
        System.out.println("成功下载了"+name);
        return true;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ThreadDemo4 td = new ThreadDemo4("1.jpg","https://wx1.sinaimg.cn/mw2000/002PnnDnly1guolj4ov35j62c02c0u0x02.jpg");
        ThreadDemo4 td1 = new ThreadDemo4("2.jpg","https://wx1.sinaimg.cn/mw2000/002PnnDnly1guolj4ov35j62c02c0u0x02.jpg");
        ThreadDemo4 td2 = new ThreadDemo4("3.jpg","https://wx1.sinaimg.cn/mw2000/002PnnDnly1guolj4ov35j62c02c0u0x02.jpg");

        //创建执行服务
        ExecutorService es = Executors.newFixedThreadPool(3);

        //提交执行
        Future<Boolean> rs1 = es.submit(td);
        Future<Boolean> rs2 = es.submit(td1);
        Future<Boolean> rs3 = es.submit(td2);

        //获取结果
        boolean r1 = rs1.get();
        boolean r2 = rs2.get();
        boolean r3 = rs3.get();

        //关闭服务
        es.shutdown();


    }
}

class WebDownloader1{
    public void downloader(String url,String name)  {
        try {
            FileUtils.copyURLToFile(new URL(url),new File(name));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

