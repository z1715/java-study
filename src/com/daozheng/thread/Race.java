package com.daozheng.thread;

import java.util.Objects;

public class Race implements Runnable{

    private static String winner;
    @Override
    public void run() {

        for (int i = 0; i < 101; i++) {
            if (Objects.equals(Thread.currentThread().getName(), "兔子") && i/50==1 ){
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName() + "跑了" + i + "步。");
            boolean signal = gameOver(i);
            if (signal){
                break;
            }
        }
    }

    private static boolean gameOver(int step){
        if(winner!=null){
            return true;
        }else {
            if(step == 100){
                winner = Thread.currentThread().getName();
                System.out.println("冠军是" + winner);
            }
            return false;
        }

    }

    public static void main(String[] args) {
        Race r = new Race();

        new Thread(r,"兔子").start();
        new Thread(r,"乌龟").start();
    }
}
