package com.daozheng.thread;


/*用Synchronized解决并发线程访问的问题
 * 要精确的锁住负责修改数据的关键代码，不要多锁*/
public class ThreadDemo6 implements Runnable {

    private  int ticket = 10;

    @Override
    public void run() {
        getTickets1();


    }

    /*成功解决同步问题的写法(不能算完全解决，去掉休眠还是不行）
     * 这是用同步代码块的方法*/
    public void getTickets() {

        while (true) {
            if (ticket <= 0) {
                break;
            }
            try {
                synchronized (this) {
                    System.out.println(Thread.currentThread().getName() + "得到了第"
                            + ticket-- + "张票");
                }
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
//这里尝试用同步方法的方式解决问题，但是代码拆分不够精准，没有解决问题
    public synchronized void getTickets1() {
        String name = Thread.currentThread().getName();
        while (true) {
            if (ticket <= 0) {
                break;
            }
            try {
                System.out.println(name + "得到了第"
                        + ticket-- + "张票");
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    //不能解决同步问题的写法
    public void getTickets2() {

        synchronized (this) {
            while (true) {
                if (ticket <= 0) {
                    break;
                }
                try {
                    System.out.println(Thread.currentThread().getName() + "得到了第"
                            + ticket-- + "张票");
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    public static void main(String[] args) {
        ThreadDemo6 td = new ThreadDemo6();

        new Thread(td, "鲤鱼").start();
        new Thread(td, "千砂都").start();
        new Thread(td, "香音").start();
    }
}
