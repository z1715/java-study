package com.daozheng.thread;

//创建线程方式一：继承Thread类，重写run（）方法，调用start开启线程
public class ThreadDemo extends Thread{
    @Override
    public void run() {
        //run()方法线程体
        for (int i = 0; i < 20; i++) {
            System.out.println("====在看视频"+i);
        }
    }

    public static void main(String[] args) {
        //main线程、主线程体
        //创建一个线程对象
        ThreadDemo t1 = new ThreadDemo();
        //调用start（）方法开启线程（两个线程交替进行），也可以用run（）（先进行run（）线程，再主线程）
        t1.start();

        for (int i = 0; i < 1000; i++) {
            System.out.println("在看代码----"+i);
        }
    }
}
