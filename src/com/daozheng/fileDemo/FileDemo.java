package com.daozheng.fileDemo;

import java.io.File;
import java.text.SimpleDateFormat;

public class FileDemo {
    public static void main(String[] args) {
        //路径写法
        /*
        E:\JS\js\1.jpg
        E:/JS/js/1.jpg
        "E:" + File.separator +"JS"+File.separator + "js/1.jpg"

        */
        File f = new File("E:" + File.separator +"JS"+File.separator + "js/1.jpg");
        long size = f.length();
        System.out.println(size);

        //File创建对象支持绝对路径，也支持相对路径
        // 相对路径一般用于定位模块中的文件  相对于工程下（默认到工程下找文件)
        // 这里的相对路径是不成功的
        //可以通过exists()方法来判断当前相对路径是基于哪个工程，现在可以判断是基于MovieSystem，而不是js
        File f1 = new File("MovieSystem/MovieSystem.iml");
        System.out.println(f1.exists());
        System.out.println(f1.length());

        //File创建对象，可以是文件，也可以是文件夹，但是拿不到文件夹大小
        File f2 = new File("E:\\2345Downloads");
        System.out.println(f2.exists());

        //获得绝对路径
        System.out.println(f1.getAbsolutePath());

        //获得File对象定义时使用的路径，（可以是相对，也可以是绝对）
        System.out.println(f1.getPath());//f1

        //获得文件的名称，带后缀
        System.out.println(f1.getName());

        //获得文件的大小，字节个数
        System.out.println(f1.length());

        //获得文件的最后修改时间，返回时间毫秒值
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(f1.lastModified()));

        //判断是否是文件、文件夹
        System.out.println(f1.isFile());
        System.out.println(f1.isDirectory());





    }



}
