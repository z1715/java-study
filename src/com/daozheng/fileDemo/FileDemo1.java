package com.daozheng.fileDemo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FileDemo1 {
    public static void main(String[] args) throws IOException {
        File f = new File("MovieSystem/src/1.jpg");
        File f1 = new File("MovieSystem/src/1.txt");
        File f2 = new File("a");
        File f3 = new File("MovieSystem/src");
        File f4 = new File("b/t.txt");
        System.out.println(f1.createNewFile());//创建文件
        System.out.println(f4.createNewFile());//创建文件
        System.out.println(f2.mkdir());//创建一级文件夹
        System.out.println(f3.mkdirs());//创建多级文件夹
        System.out.println(f1.delete());//删除文件，文件占用时也能删除
        System.out.println(f2.delete());//删除文件夹，不能删除非空文件夹

        String[] l = f3.list();
        for (String s:l) {
            System.out.println(s);
        }
        System.out.println("-------------");
        File[] l1 = f3.listFiles();


        File f5 = new File("E:/");
        recu1(new File("E:/"),"jpg");


    }
//递归的写法。 #遍历文件夹
    public static void recu1(File file,String name){
         ArrayList<String> dir1 = new ArrayList<>(){};
        if (file.listFiles()!=null) {
            for (File fi:file.listFiles()) {
                if(fi.isDirectory()){
                    /*System.out.println("----------");
                    System.out.println("这是文件夹：" + fi.getPath());
                    System.out.println("----------");*/
                    File[] ft = fi.listFiles();
                    if (ft!=null){
                    recu1(fi,name);
                    }

                }else {
                    if(fi.getName().contains(name)){
                    dir1.add(fi.getPath());

                    }

                }

            }

        } else {
            System.out.println("路径不存在");
        }
        if(dir1.size()!=0){
            System.out.println("目标文件位置如下:" );
            dir1.forEach(s -> {
                System.out.println(s+"\n");
            });
        }

    }
}
