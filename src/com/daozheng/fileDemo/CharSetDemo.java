package com.daozheng.fileDemo;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

//字符集
public class CharSetDemo {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String name = "time南is宫gold问天";
        //1，编码，把文字转换成字节
        byte[] b = name.getBytes(StandardCharsets.UTF_8);//UTF-8每个中文是3个字节
        byte[] b1 = name.getBytes("GBK");//GBK里每个汉字是2个字节
        System.out.println(b.length);
        System.out.println(Arrays.toString(b));
        StringBuffer sb = new StringBuffer();


        //2，解码，把字节转换成对应的中文形式，要注意编码前后的字符集必须一致，否则中文会乱码，不过英文不会
        String rs = new String(b);//默认是UTF-8
        String rs1 = new String(b1,"GBK");//需要调整就在后面指定字符集
        System.out.println(rs);
        System.out.println(rs1);
    }
}
