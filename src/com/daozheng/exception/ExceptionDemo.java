package com.daozheng.exception;

public class ExceptionDemo {
    public static void main(String[] args) {

        try {
            checkAge(44);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void checkAge(int age) throws AgeIllegalException{
        if(age < 0 || age > 120){
            //抛出异常
            throw new AgeIllegalException("请输入0到120之间的正整数年龄:" + age );
        }else {
            System.out.println("说的对");
        }
    }
}
