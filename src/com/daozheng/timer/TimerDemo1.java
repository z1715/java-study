package com.daozheng.timer;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/*定时器的第二种实现方式*/
public class TimerDemo1 {
    public static void main(String[] args) {
        //创建线程池
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(3);
        //调用周期处理任务的方法
        ses.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "开始跑" + new Date());
                try {
                    Thread.sleep(300000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },3,2, TimeUnit.SECONDS);
        //简化写法，ses.scheduleAtFixedRate(() -> System.out.println("在跑了"),3,2, TimeUnit.SECONDS);
        ses.scheduleAtFixedRate(() ->
                System.out.println(Thread.currentThread().getName() + "开始跑" + new Date())
        ,3,2, TimeUnit.SECONDS);
    }
}
