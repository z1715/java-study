package com.daozheng.timer;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/*了解定时器的使用*/
public class TimerDemo {
    public static void main(String[] args) {
        //1,创建定时器
        Timer t = new Timer();//定时器本身就是一个单线程
        //2,目前调用的周期处理任务的方法，下面的代表延迟3s后启动，之后每隔2s启动一次
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "在跑");
            }
        },3000,4000);
        //timer方式是单线程在多任务情况下可能会因为前面线程原因影响后面的任务

    }
}
