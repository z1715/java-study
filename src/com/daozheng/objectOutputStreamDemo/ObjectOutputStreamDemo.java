package com.daozheng.objectOutputStreamDemo;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


/*对象序列化（把对象存到磁盘里）和对象反序列化
被序列化的对象必须实现Serializable接口
加了transient修饰符的变量不会被序列化*/
public class ObjectOutputStreamDemo {
    //对象序列化到指定文件，用可选参数的形式方便序列化多个对象
    public void writerObjectList(String pathname, boolean isAdd, Object objectList) {

        try (
                OutputStream os = new FileOutputStream(pathname, isAdd);
                ObjectOutputStream oos = new ObjectOutputStream(os)
        ) {

            oos.writeObject(objectList);
            oos.writeChars("\r\n");

            oos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void writerObjectList(String pathname, boolean isAdd, Character character) {

        try (
                OutputStream os = new FileOutputStream(pathname, isAdd);
                ObjectOutputStream oos = new ObjectOutputStream(os)
        ) {

            oos.writeObject(character);
            oos.writeChars("\r\n");

            oos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void readerObject(String pathname) {
        try (
                InputStream is = new FileInputStream(pathname);
                ObjectInputStream ois = new ObjectInputStream(is)

        ) {
            Object o = ois.readObject();


                System.out.println(o);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void readerObjectList(String pathname) {
        try (
                InputStream is = new FileInputStream(pathname);
                ObjectInputStream ois = new ObjectInputStream(is)

        ) {
            List l = (ArrayList) ois.readObject();
            for (Object o : l) {
                Character c = (Character) o;
                System.out.println(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void readerObjectList1(String pathname) {
        try (
                InputStream is = new FileInputStream(pathname);
                ObjectInputStream ois = new ObjectInputStream(is)

        ) {
            Object o =null;
            while (((o =ois.readObject())!=null)){
                System.out.println(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
