package com.daozheng.objectOutputStreamDemo;

import java.io.Serializable;

public class Character implements Serializable {
    //设定序列化版本号，只有序列化版本号一致才能顺利序列化和反序列化
    public static final long serialVersionUID = 1;
    private String name;
    private double height;
    private  int money;//加了transient修饰符的变量不会被序列化
    public Character() {
    }

    public Character(String name, double height, int money) {
        this.name = name;
        this.height = height;
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }



    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", height=" + height +
                ", money=" + money +
                '}';
    }
}
