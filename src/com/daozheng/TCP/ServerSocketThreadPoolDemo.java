package com.daozheng.TCP;


import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

/*优化服务端，用线程池处理多个客户端的消息
* */
public class ServerSocketThreadPoolDemo {
    //创建一个线程池
    private static ExecutorService pool = new ThreadPoolExecutor(3, 4, 5, TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(2), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) {
        try {
            System.out.println("======服务端启动======");
            ServerSocket server = new ServerSocket(7788);
            while (true) {
                Socket receiveMsg = server.accept();
                System.out.println(receiveMsg.getRemoteSocketAddress() + "上线了^_^");
                FutureTask<String> f = new FutureTask<>(new ServerReadThread(receiveMsg));
                pool.execute(f);
                new Thread(f).start();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
