package com.daozheng.TCP;

import java.io.BufferedReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.Callable;

public class ServerReadThread implements Callable<String> {
    private Socket socket;

    public ServerReadThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public String call()  {
        try {
            InputStream is =socket.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String msg;
            while ((msg=br.readLine())!=null){
                System.out.println(socket.getRemoteSocketAddress()
                        + "发送了：" + msg);
            }
        } catch (SocketException e) {
            System.out.println(socket.getRemoteSocketAddress() + "下线了=_=");
        } catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
}
