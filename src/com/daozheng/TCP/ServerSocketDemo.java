package com.daozheng.TCP;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketDemo{
    public static void main(String[] args) throws IOException {
        System.out.println("======服务端=====");
        try {
            //1,注册服务端的端口
            ServerSocket ss = new ServerSocket(7777);
            //2，调用accept方法，等待接收客户端的Socket的连接请求，建立连接管道
            Socket socket = ss.accept();
            //3，从Socket管道里得到一个字节输入流
            InputStream is = socket.getInputStream();
            //4,把字节输入流包装成缓冲字符输入流
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String msg;
            if ((msg=br.readLine())!=null){
                System.out.println(socket.getRemoteSocketAddress() + "发送了：" + msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
