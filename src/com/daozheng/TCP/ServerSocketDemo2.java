package com.daozheng.TCP;


import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.FutureTask;

/*修改服务端，接收多个客户端的消息
* */
public class ServerSocketDemo2 {
    public static void main(String[] args) {
        try {
            System.out.println("======服务端启动======");
            ServerSocket server = new ServerSocket(7788);
            while (true) {
                Socket receiveMsg = server.accept();
                System.out.println(receiveMsg.getRemoteSocketAddress() + "上线了^_^");
                FutureTask<String> f = new FutureTask<>(new ServerReadThread(receiveMsg));
                new Thread(f).start();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
