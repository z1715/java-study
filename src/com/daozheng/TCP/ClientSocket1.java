package com.daozheng.TCP;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/*实现1对1多次发送，多次接收*/
public class ClientSocket1 {
    public static void main(String[] args) {
        try {
            Socket client = new Socket("127.0.0.1",7788);
            OutputStream sendMsg = client.getOutputStream();
            PrintStream ps = new PrintStream(sendMsg);
            Scanner sc = new Scanner(System.in);
            String msg;

            while (true){
                System.out.println("可以输入了：");
                msg = sc.nextLine();
                if ("exit".equals(msg)){
                    break;
                }
                ps.println(msg);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
