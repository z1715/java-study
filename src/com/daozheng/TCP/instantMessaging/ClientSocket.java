package com.daozheng.TCP.instantMessaging;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/*实现客户端可以同时接收和发送消息*/
public class ClientSocket {
    public static void main(String[] args) {
        try {
            Socket client = new Socket("127.0.0.1", 7788);
            OutputStream sendMsg = client.getOutputStream();
            PrintStream ps = new PrintStream(sendMsg);
            Scanner sc = new Scanner(System.in);
            String msg;
            //建立与服务器的连接
            Socket receiveMsg = new Socket("127.0.0.1", 7788);
            //读取消息
            FutureTask<String> ft = new FutureTask<>(new ClinentReaderThread(receiveMsg));
            new Thread(ft).start();

            while (true) {
                System.out.println("可以输入了：");
                msg = sc.nextLine();
                if ("exit".equals(msg)) {
                    break;
                }
                ps.println(msg);
            }





        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class ClinentReaderThread implements Callable<String> {
    private Socket socket;

    public ClinentReaderThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public String call() {
        try {
            InputStream is = socket.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String msg;
            while ((msg = br.readLine()) != null) {
                System.out.println(socket.getRemoteSocketAddress()
                        + "发送了：" + msg);
            }
        } /*catch (SocketException e) {
        System.out.println(socket.getRemoteSocketAddress() + "下线了=_=");
        }*/ catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}

