package com.daozheng.TCP.instantMessaging;




import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

/*优化服务端，把消息转发给多个客户端
* */
public class ServerSocketThreadPoolDemo {
    //1,定义一个List集合，存储当前在线用户（更进一步可以用Map，对应id和管道）
    public static List<Socket> allOnlingsocket = new ArrayList<>();
    //创建一个线程池
    private static ExecutorService pool = new ThreadPoolExecutor(3, 4, 5, TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(2), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) {
        try {
            System.out.println("======服务端启动======");
            ServerSocket server = new ServerSocket(7788);
            while (true) {
                Socket receiveMsg = server.accept();
                ServerSocketThreadPoolDemo.allOnlingsocket.add(receiveMsg);
                System.out.println(receiveMsg.getRemoteSocketAddress() + "上线了^_^"
                + new Date());
                FutureTask<String> f = new FutureTask<>(new ServerReadThread1(receiveMsg));
                pool.execute(f);
                new Thread(f).start();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

 class ServerReadThread1 implements Callable<String> {
    private Socket socket;

    public ServerReadThread1(Socket socket) {
        this.socket = socket;
    }

    @Override
    public String call()  {
        try {
            InputStream is =socket.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String msg;
            while ((msg=br.readLine())!=null){
                System.out.println(socket.getRemoteSocketAddress()
                        + "发送了：" + msg);
                //4,把收到的消息进行端口转发给所有在线的
                sendMsgToAll(msg);
            }
        } catch (SocketException e) {
            System.out.println(socket.getRemoteSocketAddress() + "下线了=_=");
            //3，把下线用户从List中移除
            ServerSocketThreadPoolDemo.allOnlingsocket.remove(socket);
        } catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

     private void sendMsgToAll(String msg) throws IOException {
         for (Socket socket1 : ServerSocketThreadPoolDemo.allOnlingsocket) {
             PrintStream ps = new PrintStream(socket1.getOutputStream());
             ps.println(msg);
             ps.flush();

         }
     }
 }

