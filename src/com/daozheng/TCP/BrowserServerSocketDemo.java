package com.daozheng.TCP;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

public class BrowserServerSocketDemo {
    private static ExecutorService pool = new ThreadPoolExecutor(3, 4, 5, TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(3), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) {
        try {
            //创建一个服务端管道
            ServerSocket ss = new ServerSocket(8080);
            Socket socket = ss.accept();
            FutureTask<String> ft = new FutureTask<>(new BrowserServerReadThread(socket));
            pool.execute(ft);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

class BrowserServerReadThread implements Callable<String>{
    private Socket socket;

    public BrowserServerReadThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public String call()  {

        try {
            //响应消息给浏览器显示
            PrintStream ps = new PrintStream(socket.getOutputStream());
            ps.println("HTTP/1.1 200 OK");
            ps.println("Content-Type:text/html;charset=UTF-8");
            ps.println();//必须发送一个空行，才可以响应数据给浏览器

            ps.println("<span> 你好啊 </span>");
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}