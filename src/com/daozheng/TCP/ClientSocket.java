package com.daozheng.TCP;


import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;

/*实现Socket1收1发*/
public class ClientSocket {
    public static void main(String[] args)  {
        try {
            //1,创建Socket对象，参数一是服务端的IP地址，参数二是服务端的端口
            Socket client = new Socket("127.0.0.1", 7777);
            //2,从Socket管道中得到一个字节输出流，负责发送数据
            OutputStream os = client.getOutputStream();
            //3,把低级流包装成高级流
            PrintStream ps = new PrintStream(os);
            //4,发送数据
            ps.println("我是TCP客户端，尝试连接。");
            ps.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
