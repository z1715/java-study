package com.daozheng.TCP;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketDemo1 {
    public static void main(String[] args) {
        try {
            System.out.println("======服务端启动======");
            ServerSocket server = new ServerSocket(7788);
            Socket receiveMsg = server.accept();
            InputStream is =receiveMsg.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String msg;
            while ((msg=br.readLine())!=null){
                System.out.println(receiveMsg.getRemoteSocketAddress()
                + "发送了：" + msg);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
