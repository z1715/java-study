package com.daozheng.dom4j;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ParseXML {
    @Test
    public void parseXML() throws Exception {
        SAXReader saxReader = new SAXReader();

        Document document = saxReader.read(
                ParseXML.class.getResourceAsStream("/com/daozheng/dom4j/connect.xml")
        );

        Element root = document.getRootElement();
        List<Element> connectEles = root.elements("member");

        List<Connect> connects = new ArrayList<>();

        for (Element connectEle : connectEles) {
            Connect connect = new Connect();
            connect.setId(Integer.parseInt(connectEle.attributeValue("id")));
            connect.setGender(connectEle.elementTextTrim("gender"));
            connect.setName(connectEle.elementTextTrim("name"));
            connect.setAge(Integer.valueOf(connectEle.elementTextTrim("age")));
            connects.add(connect);

        }
        for (Connect connect : connects) {
            System.out.println(connect);

        }
    }
}
