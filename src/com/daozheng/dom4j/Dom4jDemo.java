package com.daozheng.dom4j;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

public class Dom4jDemo {
    @Test
    public void parseXMLData() throws Exception {
        //1,创建一个解析器，代表整个dom4j框架
        SAXReader sr = new SAXReader();
        //2,把XML文件加载到内存中成为一个Document对象,read支持很多类型
        Document d = sr.read(new FileInputStream("src/com/daozheng/dom4j/connect.xml"));
        //getResourceAsStream中的/是直接去src目录下找文件
        InputStream is = Dom4jDemo.class.getResourceAsStream("/com/daozheng/dom4j/connect.xml");
        Document d1 = sr.read(is);

        //3，获取根元素对象
        Element e = d1.getRootElement();
        System.out.println("========获取根元素对象========");
        System.out.println(e.getName());

        //4，获取根元素下的所有子元素,也可以在和向elements传入想要的子元素的名称来得到子元素
        List<Element> childElements = e.elements();
        System.out.println("========获取所有子元素对象========");
        for (Element childElement : childElements) {
            System.out.println(childElement.getName());
        }
        //拿单个元素，如有多个，拿第一个
        System.out.println("========获取指定子元素对象========");
        Element e1 = e.element("father");
        System.out.println(e1.getName());
        System.out.println("========获取指定标签的文本内容========");
        System.out.println(e1.elementText("name"));
        System.out.println("========获取指定标签的文本内容（不含空格）========");
        System.out.println(e1.elementTextTrim("name"));

        System.out.println("========直接获取对应标签的属性（name）的值========");
        System.out.println(e1.attributeValue("name"));
        System.out.println("========获取对应标签的属性值========");
        Attribute a = e1.attribute("name");
        System.out.println(a.getName() + "===>" + a.getValue());

        System.out.println("========获取子元素的文本内容========");
        Element e2 = e1.element("name");
        System.out.println(e2.getText());

    }
}
