package com.daozheng.IODemo;

import java.io.*;

public class IODemo2 {


    public static void copyDemo(String pathname,String targetPath) throws Exception{
        File m = new File(pathname);
        //资源释放方式2，jdk7开始
            try(//这里只能放资源对象（实现了AutoCloseable接口的对象)，用完会自动调用资源对象的close方法关闭资源（即使出现异常）。如果放的不是资源会报错
                    InputStream is = new FileInputStream(m);
                    OutputStream os = new FileOutputStream(targetPath);

            ){
                os.write(is.readAllBytes());
                System.out.println("复制好了");

            }
        }
}
