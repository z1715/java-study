package com.daozheng.IODemo;

import java.io.*;

/*使用字节流完成文件复制*/
public class IODemo1 {
    public static void main(String[] args) throws Exception {
        copyDemo("F:\\202111181019.mp4","F:\\1133.mp4");
    }

    public static void copyDemo(String pathname,String targetPath) throws Exception{
        File m = new File(pathname);
        InputStream is =null;
        OutputStream os =null;
        /*try {
            InputStream is = new FileInputStream(m);

            OutputStream os = new FileOutputStream(targetPath);

            os.write(is.readAllBytes());
            is.close();
            os.close();
            System.out.println("复制好了");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/
        try {
             is = new FileInputStream(m);

             os = new FileOutputStream(targetPath);

            os.write(is.readAllBytes());

            System.out.println("复制好了");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            //无论代码正常执行，还是出现异常，都会执行finally中的代码，所以finally中适合释放资源，即使上面的代码中有return
            try {
                if(is!=null)is.close();//if验证是为了确保关闭流的时候流存在
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(os!=null)os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
