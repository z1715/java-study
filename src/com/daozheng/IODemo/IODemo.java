package com.daozheng.IODemo;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/*IO流体系
* 字节流
*       InputStream   字节输入流
*       OutputStream  字节输出流
* 字符流
*       Reader      字符输出流
*       Writer      字符输入流
* 以上是四大抽象类，使用时用它们的实现类
* */
public class IODemo {
    public static void main(String[] args) throws Exception {
        File f = new File("b/test.txt");
        String s = "昨天做了一个梦";
        String s1 = "，聊天记录停步去年的深秋";
        byte[] b1 = s.getBytes(StandardCharsets.UTF_8);
        byte[] b2 = s1.getBytes(StandardCharsets.UTF_8);
        boolean b = false;
        try {
            b = f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(b);
        //OutputStream fo = new FileOutputStream(f);
        OutputStream fo = new FileOutputStream(f,true);//加了ture后就把Stream流调成了追加模式，会在之前输入的内容后面继续加，不管是不是重复
        fo.write(b1);
        fo.write("\r\n".getBytes(StandardCharsets.UTF_8));//实现数据换行
        fo.write(b2);
        fo.write("\r\n".getBytes(StandardCharsets.UTF_8));

        fo.flush();//输出流写入数据后记得刷新
        fo.close();//输出流用完记得关
        InputStream fi = new FileInputStream(f);
        System.out.println((new String(fi.readAllBytes())));
        //System.out.println((new String(fi.readAllBytes(),3,6)));//3是offset，读取的字节起始位置（含），6指从起始位置向后读取多少字节


        InputStream fis = new FileInputStream(f);
        byte[] by = new byte[6];
        int len = fis.read(by);//这里是把目标文件中的字节读取到数组by中,并返回读取的字节数，read默认返回一个
        System.out.println(new String(by));


    }
}
