package com.daozheng.IODemo;

import java.io.*;

public class IODemo3 {
    public static void copyDemo(String pathname,String targetPath) throws Exception{
        new IODemo4().readerDemo1("b/test.txt");
        File m = new File(pathname);
        /*资源释放方式3，jdk9开始
        先定义资源对象，然后把资源对象名放到try后面的括号里
        *总的来说jdk7的直接使用更好用,但jdk9的方便传对象用更好 */
        InputStream is = new FileInputStream(m);
        OutputStream os = new FileOutputStream(targetPath);
        try(//这里的两个变量一定要初始化
            is;os

        ){
            os.write(is.readAllBytes());
            System.out.println("复制好了");

        }
    }
}
