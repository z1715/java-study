package com.daozheng.IODemo;

import com.daozheng.bufferedDemo.Test;

import java.io.*;

//字符输入转换流，继承Reader，转换流可以用来解决乱码的问题
public class IODemo5 {
    public static void main(String[] args) {
        String s = "试试字符输出转换流";
        new IODemo5().writer(Test.PATH_NAME,s,"UTF-8");
        new IODemo5().reader(Test.PATH_NAME,"UTF-8");
    }
    public void reader(String pathname,String charset){
        try(InputStream is = new FileInputStream(pathname);
            //把特定的流，用指定的字符集进行转换
            Reader isr = new InputStreamReader(is, charset);
            BufferedReader br = new BufferedReader(isr);
            ) {
            String s;
            while ((s=br.readLine())!=null){
                System.out.println(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void writer(String pathname,String content,String charset){
        //不加true会清空内容并重新写入
        try(OutputStream os = new FileOutputStream(pathname,true);
            //把特定的流，用指定的字符集进行输出
            Writer w = new OutputStreamWriter(os, charset);
            BufferedWriter br = new BufferedWriter(w);
        ) {
            br.write(content);
            br.newLine();
            br.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
