package com.daozheng.IODemo;

import java.io.*;

//字符输入流、字符输出流
public class IODemo4 {
    public static void main(String[] args) throws Exception {
        IODemo4 io = new IODemo4();
        String s = "不要停下来";
        io.writerDemo("b/test.txt",s);
        io.readerDemo1("b/test.txt");//类的一般方法必须在类实例化后才能使用，只有静态方法才能直接调用
    }

//单个字符读取
    public  static void readerDemo(String pathname) throws Exception {
        File f = new File(pathname);
        Reader r = new FileReader(f);
        int code ;
        while ((code=r.read())!=-1){
            System.out.print((char) code);
        }

    }
    //把字符读取到字符数组中
    public  void readerDemo1(String pathname) throws Exception {
        File f = new File(pathname);
        Reader r = new FileReader(f);
        try(r) {
            int len ;
            char[] rs = new char[1024];//装这么多字符
            while ((len=r.read(rs))!=-1){
                String rs1 = new String(rs,0,len);
                System.out.print(rs1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //把字符输出到文件中
    public void writerDemo(String pathname,String content) throws Exception {
        Writer w = new FileWriter(pathname,true);
        try (w){
            w.write("\r\n");
            w.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
