package com.daozheng.stringbuffer;

public class StringbufferDemo {
    public void test(String s){
        StringBuffer sb = new StringBuffer("构造器里放字符串会以该字符串开头：");
        sb.append(s);
        System.out.println(sb);
    }
    public void test1(String s){
        StringBuffer sb = new StringBuffer("构造器里可以放字符序列，也就是" +
                "CharBuffer, Segment, String, StringBuffer, StringBuilder这些啦");
        sb.append(s);
        System.out.println(sb);
    }
    public void test2(String s){
        StringBuffer sb = new StringBuffer();
        sb.append(s);
        System.out.println("构造器放数字是设定初始的字节容量，不放是默认16，好像没什么影响。" + sb);
    }
}
