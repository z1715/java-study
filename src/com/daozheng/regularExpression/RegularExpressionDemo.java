package com.daozheng.regularExpression;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressionDemo {
    public static void main(String[] args) throws Exception {
        File f = new File("b/test.txt");
        InputStream is = new FileInputStream(f);
        String s = new String(is.readAllBytes());
        String s1 = "前天";

        Pattern p =Pattern.compile("昨天");
        Matcher m = p.matcher(s);
        String s2 = m.replaceAll(s1);
        String pattern = "天";

        boolean isMatch = Pattern.matches(pattern, s);

        System.out.println(isMatch);
        System.out.println(s2);
    }
}
