package com.daozheng.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/*JDBC快速入门*/
public class JDBCDemo {
    public static void main(String[] args) throws Exception {
        //1,注册驱动
        Class.forName("com.mysql.jdbc.Driver");

        //url后面的继续加参数键值对是用&，如"jdbc:mysql://127.0.0.1:3306/users?键值对&键值对"
        String url = "jdbc:mysql://127.0.0.1:3306/users?useSSL=false";
        String userName = "root";
        String password = "12345";
        //2,获取连接，JDBCDemo
        Connection conn = DriverManager.getConnection(url, userName, password);

        //3,定义sql语句
        String sql = "alter table te_users rename to tb_users";
        //4,获取执行sql对象
        Statement statement = conn.createStatement();
        //5,执行sql
        int i = statement.executeUpdate(sql);

        System.out.println(i);

        //6,释放资源
        statement.close();
        conn.close();

    }
}
