package com.daozheng.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*了解Connection的事务开启，提交，回滚*/
public class JDBCAffairsDemo {
    public static void main(String[] args) throws Exception {
        //1,注册驱动
        Class.forName("com.mysql.jdbc.Driver");

        //url后面的继续加参数键值对是用&，如"jdbc:mysql://127.0.0.1:3306/users?键值对&键值对"
        String url = "jdbc:mysql://127.0.0.1:3306/users?useSSL=false";
        String userName = "root";
        String password = "12345";
        //2,获取连接，JDBCDemo
        Connection conn = DriverManager.getConnection(url, userName, password);

        //3,定义sql语句
        String sql = "alter table tb_users rename to te_users";
        //4,获取执行sql对象
        Statement statement = null;

        try {
            //开启事务
            conn.setAutoCommit(false);
            statement = conn.createStatement();

            //5,执行sql
            int i = statement.executeUpdate(sql);
            System.out.println(i);
            //提交事务
            conn.commit();
        } catch (SQLException e) {
            //事务回滚
            conn.rollback();
            e.printStackTrace();
        }



        //6,释放资源
        statement.close();
        conn.close();

    }
}
