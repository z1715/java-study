package com.daozheng.JDBC;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.junit.Test;
import pojo.Brand;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class BrandTest {
    /*1,是否需要提供参数：不用
    * 2，是否需要返回数据：需要返回全部内容*/
    @Test
    public void queryTest() throws Exception {
        Properties prop = new Properties();
        prop.load(new FileInputStream("src/druid.properties"));
        //System.out.println(System.getProperty("user.dir"));
        DataSource dataSource = DruidDataSourceFactory.createDataSource(prop);

        //获取数据库连接
        Connection connection = dataSource.getConnection();
        
        String sql = "select * from users.tb_brand ;";
        
        //获取pstmt对象
        PreparedStatement pstmt = connection.prepareStatement(sql);
        
        //设置参数

        ResultSet rs = pstmt.executeQuery();
        List<Brand> listBrand = new ArrayList<>();
        //处理结果对象，封装到一个集合里
        while (rs.next()){
            Brand b =new Brand();
            b.setId(rs.getInt("id"));
            b.setBrandName(rs.getString("brand_name"));
            b.setCompanyName(rs.getString("company_name"));
            b.setOrderBy(rs.getInt("orderBy"));
            b.setDescription(rs.getString("description"));
            b.setStatus(rs.getInt("status1"));
            listBrand.add(b);
        }
        System.out.println(listBrand);
        //释放资源
        rs.close();
        pstmt.close();
        connection.close();

    }
    /*1,是否需要提供参数：需要提供除id以外的所有参数
     * 2，是否需要返回数据：需要返回boolean判断结果是否成功执行*/
    @Test
    public void addTest() throws Exception {
        Properties prop = new Properties();
        prop.load(new FileInputStream("src/druid.properties"));
        //System.out.println(System.getProperty("user.dir"));
        DataSource dataSource = DruidDataSourceFactory.createDataSource(prop);

        //获取数据库连接
        Connection connection = dataSource.getConnection();

        String sql = "insert into users.tb_brand(brand_name, company_name, orderBy, description, status1) " +
                "values (?,?,?,?,?);";

        //获取pstmt对象
        PreparedStatement pstmt = connection.prepareStatement(sql);

        //设置参数
        pstmt.setString(1,"白蛇");
        pstmt.setString(2,"白蛇CD");
        pstmt.setInt(3,999);
        pstmt.setString(4,"坚不可摧");
        pstmt.setInt(5,0);

        int i = pstmt.executeUpdate();
        if (i!=0){
            System.out.println("更新成功！");
        }else {
            System.out.println("更新失败！");
        }


        //释放资源

        pstmt.close();
        connection.close();

    }

    /*1，是否需要提供参数：需要提供所有参数
    * 2，是否需要返回数据：需要返回boolean*/
    @Test
    public void updateTest() throws Exception{
        Properties properties = new Properties();
        properties.load(new FileInputStream("src/druid.properties"));

        DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);

        Connection connection = dataSource.getConnection();

        String sql = "update users.tb_brand\n" +
                " set brand_name = ?,\n"+
                " company_name = ?,\n"+
                " orderBy = ?,\n"+
                " description = ?,\n"+
                " status1 = ?\n"+
                " where id = ?;";
        System.out.println(sql);
        PreparedStatement preparedStatement = connection.prepareStatement(sql);

        preparedStatement.setString(1,"白蛇");
        preparedStatement.setString(2,"白蛇CD");
        preparedStatement.setInt(3,99);
        preparedStatement.setString(4,"品鼍");
        preparedStatement.setInt(5,0);
        preparedStatement.setInt(6,4);

        int i = preparedStatement.executeUpdate();
        if (i!=0){
            System.out.println("修改成功！");
        }else {
            System.out.println("修改失败！");
        }

        preparedStatement.close();
        connection.close();
    }
    /*1，是否需要提供参数：需要提供id
     * 2，是否需要返回数据：需要返回boolean*/
    @Test
    public void deleteTest() throws Exception{
        Properties properties = new Properties();
        properties.load(new FileInputStream("src/druid.properties"));

        DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);

        Connection connection = dataSource.getConnection();

        String sql = "delete from users.tb_brand where id = ?;";
        System.out.println(sql);
        PreparedStatement preparedStatement = connection.prepareStatement(sql);


        preparedStatement.setInt(1,4);

        int i = preparedStatement.executeUpdate();
        if (i!=0){
            System.out.println("删除成功！");
        }else {
            System.out.println("删除失败！");
        }

        preparedStatement.close();
        connection.close();
    }
}
