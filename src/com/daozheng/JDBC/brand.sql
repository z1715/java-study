-- 删除tb_brand表
drop table if exists tb_brand;
-- 创建tb_brand表
create table tb_brand(
    -- id 主键
    id int primary key auto_increment,
    -- 品牌名称
    brand_name varchar(20),
    -- 企业名称
    company_name varchar(20),
    -- 排序字段
    orderBy int,
    -- 描述信息
    description varchar(20),
    -- 状态：0-禁用，1-启用
    status1 int
);
-- 添加数据
insert into tb_brand(brand_name, company_name, orderBy, description, status1)
values ('阿帕茶','阿帕茶酒水',5,'量大管饱',0),
       ('石之自由','徐伦制衣',58,'量体裁衣',1),
       ('东云','东云研究所',50,'超乎想象',0);

select id,brand_name, company_name, orderBy, description, status1 from tb_brand;