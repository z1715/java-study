package com.daozheng.IP;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAdressDemo {
    public static void main(String[] args) throws Exception {
        InetAddress ia = InetAddress.getLocalHost();
        System.out.println(ia);
        System.out.println(ia.getHostAddress());
        System.out.println(ia.getHostName());
        System.out.println(ia.getAddress());
    }
}
