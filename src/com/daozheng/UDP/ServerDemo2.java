package com.daozheng.UDP;

import java.net.*;

/*这组播接收端没写好，跑不起来*/
public class ServerDemo2 {
    public static void main(String[] args) throws Exception{
        System.out.println("==========服务端启动==========");
        InetSocketAddress isa = new InetSocketAddress("226.0.1.1",9999);
       // NetworkInterface.getByIndex(1);
        byte[] content1 = new byte[1024*64];
        //接收端
       // DatagramSocket rs = new DatagramSocket(9999);
        MulticastSocket ms = new MulticastSocket(9999);
        ms.joinGroup(isa,NetworkInterface.getByInetAddress(InetAddress.getByName("127.0.0.1")));
        DatagramPacket rec = new DatagramPacket(content1,content1.length);
        //接收包
        while (true) {


            //等待接收数据
            ms.receive(rec);

            /*不调整结果是会输出多余的空格
             * 用接收端的getLength方法得到实际接收数据的长度*/
            int len = rec.getLength();
            String r = new String(content1,0,len);

            System.out.println("接收到的结果是：" + r);
        }


    }
}
