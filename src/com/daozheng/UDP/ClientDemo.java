package com.daozheng.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;

public class ClientDemo {
    public static void main(String[] args) throws Exception{
        byte[] content = "用字符串的getByte()方法可以得到字符数组".getBytes(StandardCharsets.UTF_8);

        InetAddress ia = InetAddress.getLocalHost();
        //发送包
        DatagramPacket post = new DatagramPacket(content,content.length,ia,9999);

        //发送端
        DatagramSocket ds = new DatagramSocket();
        //发送数据
        ds.send(post);

        //用完关掉
        ds.close();

    }
}
