package com.daozheng.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

//测试的时候先启动服务端（接收端）
public class ServerDemo {
    public static void main(String[] args) throws Exception{
        System.out.println("==========服务端启动==========");
        byte[] content1 = new byte[1024*64];
        //接收包
        DatagramPacket rec = new DatagramPacket(content1,content1.length);
        //接收端
        DatagramSocket rs = new DatagramSocket(9999);
        //等待接收数据
        rs.receive(rec);

        /*不调整结果是会输出多余的空格
        * 用接收端的getLength方法得到实际接收数据的长度*/
        int len = rec.getLength();
        String r = new String(content1,0,len);

        System.out.println("接收到的结果是：" + r);

        //获得发送端的ip和端口
        String address = rec.getSocketAddress().toString();
        System.out.println("发送方的ip地址和端口是：" + address);
        rs.close();
    }
}
