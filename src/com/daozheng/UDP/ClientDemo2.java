package com.daozheng.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;


/*实现组播
* 如果要实现局域网内组播，发送端需要把ip地址改成224.0.0.0~239.255.255.255中的一个，端口改成9999
 * 接收端要绑定发送端的组播IP，把端口改成9999就可以收到消息了
 * DatagramSocket的子类MulticastSocket可以在接收端绑定组播IP*/
public class ClientDemo2 {
    public static void main(String[] args) throws Exception{
        byte[] content = "用字符串的getByte()方法可以得到字符数组".getBytes(StandardCharsets.UTF_8);

        InetAddress ia = InetAddress.getByName("226.0.1.1");
        DatagramSocket ds = new DatagramSocket();
       // System.out.println(NetworkInterface.getByInetAddress(InetAddress.getLocalHost()) );
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("======发送端启动=====");
            String content1 = sc.nextLine();
            if ("exit".equals(content1)){
                System.out.println("那再见了。");
                ds.close();
                break;
            }

            DatagramPacket post = new DatagramPacket(content1.getBytes(),
                    content1.getBytes().length,ia,9999);

            ds.send(post);
        }

        //用完关掉
        ds.close();

    }
}

