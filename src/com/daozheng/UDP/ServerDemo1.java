package com.daozheng.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ServerDemo1 {
    public static void main(String[] args) throws Exception{
        System.out.println("==========服务端启动==========");
        byte[] content1 = new byte[1024*64];
        //接收端
        DatagramSocket rs = new DatagramSocket(9999);
        //接收包
        while (true) {
            DatagramPacket rec = new DatagramPacket(content1,content1.length);

            //等待接收数据
            rs.receive(rec);

            /*不调整结果是会输出多余的空格
             * 用接收端的getLength方法得到实际接收数据的长度*/
            int len = rec.getLength();
            String r = new String(content1,0,len);

            System.out.println("接收到的结果是：" + r);
        }


    }
}
