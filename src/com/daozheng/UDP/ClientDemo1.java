package com.daozheng.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ClientDemo1 {
    public static void main(String[] args) throws Exception{
        byte[] content = "用字符串的getByte()方法可以得到字符数组".getBytes(StandardCharsets.UTF_8);

        InetAddress ia = InetAddress.getLocalHost();
        DatagramSocket ds = new DatagramSocket();
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("======发送端启动=====");
            String content1 = sc.nextLine();
            if ("exit".equals(content1)){
                System.out.println("那再见了。");
                ds.close();
                break;
            }
            /*如果要实现局域网内广播，需要把ip地址改成255.255.255.255，端口改成9999
            * 这样广播主机所在网段的其他主机的程序接收端只要把端口改成9999就可以收到消息了*/
            DatagramPacket post = new DatagramPacket(content1.getBytes(),
                    content1.getBytes().length,ia,9999);

            ds.send(post);
        }

        //用完关掉
        ds.close();

    }
}
