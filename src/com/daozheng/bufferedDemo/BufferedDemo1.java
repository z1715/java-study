package com.daozheng.bufferedDemo;

import java.io.*;

//缓冲字符输入、输出流
public class BufferedDemo1 {
    //数组读取
    public void read(String pathname) throws Exception{
        long startTime = System.currentTimeMillis();
        Reader r = new FileReader(pathname);
        BufferedReader bf = new BufferedReader(r);
        try(r;bf) {
            int len ;
            char[] rs = new char[1024];//装这么多字符
            while ((len=r.read(rs))!=-1){
                String rs1 = new String(rs,0,len);
                System.out.print(rs1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("花了"+ (startTime - endTime) + "ms");
    }
//BufferedReader独有的按行读取readLine()，好像不会自动换行
    public void read1(String pathname) throws Exception{
        long startTime = System.currentTimeMillis();
        Reader r = new FileReader(pathname);
        BufferedReader bf = new BufferedReader(r);
        try(r;bf) {
            String rs1;
            while ((rs1 = bf.readLine())!=null){

                System.out.println(rs1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("花了"+ (startTime - endTime) + "ms");
    }
    //写入内容,BufferedWriter有独有的换行功能newLine()
    public void write(String pathname,String content) throws Exception {
        Writer w = new FileWriter(pathname,true);
        BufferedWriter bw = new BufferedWriter(w);
        try (w;bw){
            bw.newLine();
            bw.write(content);
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
