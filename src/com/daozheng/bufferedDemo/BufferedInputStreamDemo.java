package com.daozheng.bufferedDemo;

import java.io.*;

//字节缓冲输出流、字节缓冲输入流
public class BufferedInputStreamDemo {

    public void bufferCopy (String pathname,String targetPath) throws Exception {

        /*就是把一般的字节输入输出流放到缓冲字节输入输出流 */
        InputStream is = new FileInputStream(pathname);
        BufferedInputStream bi = new BufferedInputStream(is);
        OutputStream os = new FileOutputStream(targetPath);
        BufferedOutputStream bo = new BufferedOutputStream(os);
        try (//这里的两个变量一定要初始化
             is; os; bi; bo

        ) {
            int len;
            byte[] rs = new byte[1024];//装这么多字符
            while ((len = bi.read(rs)) != -1) {
                String rs1 = new String(rs, 0, len);
                System.out.print(rs1);
                System.out.println("复制好了");

            }
        }
    }
}
