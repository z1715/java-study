package com.daozheng.bufferedDemo;

import com.daozheng.objectOutputStreamDemo.Character;
import com.daozheng.objectOutputStreamDemo.ObjectOutputStreamDemo;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static final String PATH_NAME ="b/test.txt";
    public static final String PATH_OBJECT ="b/t.txt";


    public static void main(String[] args) throws Exception {
        /*String s = "试试缓冲字符输入输出流";
        //new BufferedInputStreamDemo().bufferCopy("C:\\Users\\Administrator\\Downloads\\Jianying_pro_2_4_0_5338_jianyingpro_sougou.exe","F:\\jj.exe");
        BufferedDemo1 bd = new BufferedDemo1();
        //  bd.read("b/test.txt");
        bd.write(PATH_NAME,s);
        bd.read1(PATH_NAME);*/
        Character c = new Character("玛嘉烈",178.0,345);
        Character c1 = new Character("银灰",188.0,345);
        Character c2 = new Character("塞雷娅",175.0,345);
        List<Character> l = new ArrayList<>();
        l.add(c);
        l.add(c1);
        l.add(c2);
        ObjectOutputStreamDemo oosd = new ObjectOutputStreamDemo();
       // oosd.writerObjectList(PATH_OBJECT,true,l);
        oosd.writerObjectList(PATH_OBJECT,true,c);
        oosd.writerObjectList(PATH_OBJECT,true,c1);
        oosd.writerObjectList(PATH_OBJECT,true,c2);

       // oosd.readerObjectList1(PATH_OBJECT);
        oosd.readerObject(PATH_OBJECT);


    }
}
